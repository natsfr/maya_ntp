`timescale 1ns / 1ps
`default_nettype none
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:42:30 09/01/2016 
// Design Name: 
// Module Name:    top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input wire clk50,
    output wire mdc,
    inout wire mdio,
	 output wire uart_tx
    );

	 // BUF clk50
	 wire clk50_buf;
	 IBUFG in_clk50(.I(clk50), .O(clk50_buf));

	//General parameters
	localparam SECOND					= 50000000;
	reg [25:0]second_cnt 			= 0;
	
	// ================ UART =================== //
	reg [7:0]dsend = 8'd52;
	reg u_start = 0;
	
	wire u_done;

	uart_tx #(.BAUD(9600), .CLK_SPEED(50000000))
				uart_debug(.clk(clk50_buf), .start(u_start),
				.dsend(dsend), .bit_out(uart_tx), .done(u_done));
	
	// ================ MDIO =================== //
	reg phy_rd = 0;
	reg phy_wr = 0;
	
	wire mdio_busy;
	
	reg [4:0]reg_addr = 0;
	reg [15:0]data_wr = 0;
	wire [15:0]data_rd;

	MDIO_transceiver mdio_88e1512(.clk(clk50_buf), .mdio(mdio), .mdc(mdc),
							.phy_rd(phy_rd), .phy_wr(phy_wr), .mdio_busy(mdio_busy),
							.reg_addr(reg_addr), .data_wr(data_wr), .data_rd(data_rd));
							
	
	// FSM module control
	wire mdio_ready = !phy_wr && !phy_rd && !mdio_busy;
	wire uart_ready = u_done && !u_start;
	
	reg [2:0] 	main_fsm_state = 0;
	reg [2:0] 	fsm_cnt = 0;
	
	// MDIO FSM
	
	reg [1:0]sync_fsm_state = 0;
	
	localparam IDLE					= 0;
	localparam READ_ID1				= 1;
	localparam SET_PAGE0				= 2;
	localparam PUSH_DATA				= 3;

	always @(posedge clk50_buf)
	begin
		case(sync_fsm_state)
			IDLE: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE0;
				end
			end
			
			SET_PAGE0: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_wr <= 1'b1;
					reg_addr <= 5'd22;
					data_wr <= 16'd1;
					sync_fsm_state <= READ_ID1;
				end
			end
			
			READ_ID1: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_rd <= 1'b1;
					reg_addr <= 5'd21;
					sync_fsm_state <= PUSH_DATA;
				end
			end
			
			PUSH_DATA: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE0;
				end
			end
		endcase
	end
	
	// UART FSM	
	
	localparam SEND_MSB				= 1;
	localparam SEND_LSB				= 2;
	
	always @(posedge clk50_buf)
	begin
		case(main_fsm_state)
			IDLE: begin
				u_start <= 0;
				if(uart_ready) begin
					main_fsm_state <= SEND_MSB;
				end
			end
			
			SEND_MSB: begin
				u_start <= 0;
				if(uart_ready) begin
					dsend <= data_rd[15:8];
					u_start <= 1;
					main_fsm_state <= SEND_LSB;
				end
			end
			
			SEND_LSB: begin
				u_start <= 0;
				if(uart_ready) begin
					u_start <= 1;
					dsend <= data_rd[7:0];
					main_fsm_state <= IDLE;
				end
			end
		endcase
	end
endmodule
