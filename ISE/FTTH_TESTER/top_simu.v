`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:58:22 12/23/2016 
// Design Name: 
// Module Name:    top_simu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top_simu(
    );

reg clk = 0;

always #1 clk = ~clk;

top fpga_top(.clk50(clk), .RXC(clk));

endmodule
