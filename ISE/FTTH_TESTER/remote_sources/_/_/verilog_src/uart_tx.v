module uart_tx(clk, start, dsend, bit_out, done);
	
	// Parameters
	
	parameter BAUD = 115200;
	parameter CLK_SPEED = 50000000;
	
	parameter STOP_BITS = 1; // Number of stop bits
	
	parameter DATA_SIZE = 8; // Number of data bit
	
	parameter PAR_TYPE = "N"; // N|O|E|M|S
	
	localparam PARITY = (PAR_TYPE == "N") ? 0:1;
	localparam DIVIDER = (CLK_SPEED/BAUD);
	
	// IO Declaration
	
	input wire clk;
	input wire start;	
	input wire [DATA_SIZE-1:0]dsend;
	
	output reg bit_out = 1;
	output reg done = 1;
	
	// Generate TX Clock
	
	reg uart_edge = 0;
	reg [31:0]clk_counter = 0;
	
	reg [DATA_SIZE-1:0]data_ff = 0;
	
	wire xpar = ^data_ff;
	
	always @(posedge clk)
	begin
		uart_edge <= 0;
		clk_counter <= clk_counter + 1;
		if(clk_counter == (DIVIDER - 1)) begin
			uart_edge <= 1;
			clk_counter <= 0;
		end
	end
	
	// UART FSM
	
	localparam IDLE = 0;
	localparam SOF = 1;
	localparam SEND_DATA = 2;
	localparam PARBIT = 3;
	localparam STOPF = 4;
	
	reg [2:0]state = 0;    // states of the uart fsm
	reg [7:0]uart_cnt = 0; // counter meaning depends on state
	reg run = 0;
	
	always @(posedge clk)
	begin
		if(start == 1 && done == 1) begin 
			run <= 1'b1;
			data_ff <= dsend;
			done <= 1'b0;
		end
		if(uart_edge) begin
			case(state)
				IDLE: begin
					uart_cnt <= 4'd0;
					if(run) begin 
						state <= SOF;
					end
				end
				SOF: begin
					bit_out <= 1'b0;
					state <= SEND_DATA;
				end
				SEND_DATA: begin
					if(uart_cnt >= DATA_SIZE - 1) begin 
						if(PARITY)
							state <= PARBIT;
						else
							state <= STOPF;
						uart_cnt <= 4'd0;
					end else begin
						uart_cnt <= uart_cnt + 4'd1;
					end
					bit_out <= data_ff[uart_cnt];
				end
				PARBIT: begin
					if(PAR_TYPE == "O")
						bit_out <= ~xpar;
					else if(PAR_TYPE == "E")
						bit_out <= xpar;
					else if(PAR_TYPE == "M")
						bit_out <= 1'b1;
					else if(PAR_TYPE == "S")
						bit_out <= 1'b0;
					state <= STOPF;
					uart_cnt <= 4'd0;
				end
				STOPF: begin
					if(uart_cnt < STOP_BITS) begin
						bit_out <= 1'b1;
						uart_cnt <= uart_cnt + 4'd1;
					end else begin
						state <= IDLE;
						done <= 1'b1;
						run <= 0;
					end
				end
				default: state <= IDLE; // Never go there !
			endcase
		end
	end
	
endmodule