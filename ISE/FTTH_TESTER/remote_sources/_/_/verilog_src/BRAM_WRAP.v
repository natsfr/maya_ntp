module BRAM_WRAP(clk, en, addr, we, din, dout,
				clkb, enb, addrb, doutb);
	
	`include "generic_macro.vh"
	
	parameter WIDTH = 8;
	parameter DEPTH = 1024;
	
	localparam ADDR_BITS = 11;//`CLOG2(DEPTH);
	
	parameter BLOCK = 1;
	
	parameter REGISTER = 1;
	parameter REG_STAGE = 1;
	
	parameter FILE = "";
	
	parameter INIT_VAL = {WIDTH{1'b0}};
	
	parameter DUALP = 1;
	
	input wire 						clk;
	input wire 						en;
	input wire [ADDR_BITS-1:0]		addr;
	input wire 						we;
	input wire [WIDTH-1:0]			din;
	output wire [WIDTH-1:0]			dout;
	
	input wire						clkb;
	input wire						enb;
	input wire [ADDR_BITS-1:0]		addrb;
	output wire [WIDTH-1:0]			doutb;
	
	localparam style = BLOCK ? "block" : "distributed";
	
	(* RAM_STYLE = style *) reg [WIDTH-1:0] mem[0:DEPTH-1];
	
	reg [WIDTH-1:0]					dout_direct = 0;
	reg [WIDTH-1:0]					dout_ff = 0;
	
	reg [WIDTH-1:0]					doutb_direct = 0;
	reg [WIDTH-1:0]					doutb_ff = 0;
	
	initial begin
		if (BLOCK && !REGISTER) begin
			$display("Error: Block Ram is better with a register");
			$finish;
		end
	end
	
	generate
		
		always @(posedge clk) begin
			if(we && en) mem[addr] <= din;
		end
		
		if(REGISTER) begin
			always @(posedge clk) begin
				dout_ff <= dout_direct;
				if(en) dout_direct <= mem[addr];
			end
		end else begin
			always @* dout_direct = mem[addr];
		end
		
		if(REG_STAGE == 2)
			assign dout = dout_ff;
		else 
			assign dout = dout_direct;
		
	endgenerate
	
	generate
		if(DUALP) begin
			
			if(REGISTER) begin
				always @(posedge clkb) begin
					doutb_ff <= doutb_direct;
					if(enb) doutb_direct <= mem[addrb];
				end
			end else begin
				always @* doutb_direct = mem[addrb];
			end
			
			if(REG_STAGE == 2)
				assign doutb = doutb_ff;
			else
				assign doutb = doutb_direct;
		end
	endgenerate
	
	integer i;
	generate
		initial begin
			
			if(FILE != "")
				$readmemh(FILE, mem);
			else begin
				for(i=0; i < DEPTH; i = i+1)
					mem[i] <= INIT_VAL[WIDTH-1:0];
			end
			
		end
	endgenerate
endmodule