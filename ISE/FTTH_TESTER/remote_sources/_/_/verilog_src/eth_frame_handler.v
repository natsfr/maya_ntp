`timescale 1ns / 1ps

module udld_frame_handler(
	// Main clk domain
	clk,

	// RGMII Physical
	RXC, RXD, RXCTL,
	
	// BRAM Access
	bram_wr_en, bram_index, bram_data,
	
	// Signaling
	valid_frame_rx, frame_used,
	
	// debug wire
	led0, led1
	
    );

	input wire clk;
	
	input wire RXC;
	input wire [3:0]RXD;
	input wire RXCTL;
	
	output reg bram_wr_en = 0;
	output reg [10:0]bram_index = 0;
	output reg [7:0]bram_data = 0;
	
	output reg valid_frame_rx = 0;
	input wire frame_used;
	
	output reg led0 = 0;
	output reg led1 = 0;

	// RGMII to GMII

	wire [7:0]gmii_data_rx;
	wire gmii_data_rx_valid;
	wire gmii_data_rx_err;
	
	rgmii_txer rgmii_to_gmii(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL),
									 .GMII_RXD(gmii_data_rx), .GMII_RXDV(gmii_data_rx_valid), .GMII_RXER(gmii_data_rx_err));
	
	// Frame storage

	// Ethernet state machine
	
	parameter ETH_PREAMBLE						= 8'b01010101; // 0x55
	parameter ETH_SFD								= 8'b11010101; // 0xD5
	parameter ETH2_SIG							= 16'd1536; // ETH2 ethertype
	parameter SNAP_PAYLOAD_START				= 16'hAAAA;
	//parameter DEST_MAC_UDLD						= 48'h01000CCCCCCC; // UDLD Multicast MAC
	parameter LLC_CTRL_FIELD					= 8'h03;
	parameter ORG_ID								= 24'h00000C; // Cisco
	parameter HDLC_PROT_TYPE					= 16'h0111; // UDLD
	
	// Use a wire because paramters are limited to 32bit... fucking shit !
	wire 		 [47:0]DEST_MAC_UDLD						= 48'h01000CCCCCCC; // UDLD Multicast MAC
	
	
	localparam RX_IDLE						= 0;
	localparam RX_DATA						= 1;
	localparam RX_DROP						= 2;
	localparam RX_PARKING					= 3;
	
	reg [3:0]rx_state = RX_IDLE;
	
	reg [2:0]pre_cnt = 0;
	reg [15:0]byte_cnt = 0;
	
	reg [15:0]frame_size = 0;
	
	reg [47:0]dest_mac = 0;
	reg [47:0]src_mac = 0;
	reg [15:0]ethertype = 0;
	reg [15:0]llc_snap = 0;
	reg [7:0]ctrl_field = 0;
	reg [23:0]org_id = 0;
	reg [15:0]HDLC_prot_type = 0;
	
	reg [5:0]good_flag = 0;
	
	always @(posedge RXC)
	begin
	
		if(frame_used) valid_frame_rx <= 0;
	
		case(rx_state)
	
			RX_IDLE: begin
				good_flag <= 1;
				if(gmii_data_rx_valid) begin
				
					if(gmii_data_rx == ETH_PREAMBLE) begin
					
					end else if(gmii_data_rx == ETH_SFD) begin
						led0 <= 0;
						led1 <= 0;
						pre_cnt <= 0;
						rx_state <= RX_DATA;
					end
					
				end else begin
					
					if(gmii_data_rx_err) begin // False Carrier
					
					end else begin
						
					end
					
				end
			end
			
			RX_DATA: begin
				bram_wr_en <= 0;
				bram_data <= 0;
				byte_cnt <= 0;
				if(gmii_data_rx_valid) begin
					led0 <= 0;
					led1 <= 1;
					if(gmii_data_rx_err) begin
						rx_state <= RX_DROP;
						led0 <= 1;
						led1 <= 1;
					end else begin
						
						// store data in bram
						byte_cnt <= byte_cnt + 1;
						bram_wr_en <= 1;
						bram_index <= byte_cnt;
						bram_data <= gmii_data_rx;
						
						// valid special values
						if(byte_cnt < 6) begin // Destination MAC Address
							dest_mac <= { dest_mac[39:0], gmii_data_rx };
						end else if(byte_cnt < 12) begin // Source MAC Address
							if(dest_mac == DEST_MAC_UDLD) // NOT the good arp dest
								good_flag[0] <= 1;
							src_mac <= { src_mac[39:0], gmii_data_rx };
						end else if(byte_cnt < 14) begin
							ethertype <= { ethertype[7:0], gmii_data_rx};
						end else if(byte_cnt < 16) begin // Check LLC SNAP header
							if(ethertype < 16'd1500) // TOO big we want LLC SNAP packet
								good_flag[1] <= 1;
							llc_snap <= { llc_snap[7:0], gmii_data_rx};
						end else if(byte_cnt < 17) begin
							if(llc_snap == SNAP_PAYLOAD_START)
								good_flag[2] <= 1;
							ctrl_field <= gmii_data_rx;
						end else if(byte_cnt < 20) begin
							if(ctrl_field == LLC_CTRL_FIELD)
								good_flag[3] <= 1;
							org_id <= {org_id[15:0], gmii_data_rx};
						end else if(byte_cnt < 22) begin
							if(org_id == ORG_ID)
								good_flag[4] <= 1;
							HDLC_prot_type <= { HDLC_prot_type[7:0], gmii_data_rx};
						end else begin
							if(HDLC_prot_type == HDLC_PROT_TYPE)
								good_flag[5] <= 1;
						
						end
					end
				end else begin
					if(&good_flag) begin
						valid_frame_rx <= 1;
						rx_state <= RX_PARKING;
					end else begin
						valid_frame_rx <= 0;
						rx_state <= RX_IDLE;
					end
				end
			end
			
			RX_DROP: begin
				if(!gmii_data_rx_valid) begin
					rx_state <= RX_IDLE;
					byte_cnt <= 0;
					bram_wr_en <= 0;
					bram_index <= 0;
					bram_data <= 0;
				end
			end
			
			RX_PARKING: begin
				if(frame_used) begin
					valid_frame_rx <= 0;
					rx_state <= RX_IDLE;
					byte_cnt <= 0;
					led0 <= 0;
					led1 <= 0;
					byte_cnt <= 0;
					bram_wr_en <= 0;
					bram_index <= 0;
					bram_data <= 0;
				end
			end
		endcase
	
	end
	
	// Cross Clock Domain FIFO

endmodule
