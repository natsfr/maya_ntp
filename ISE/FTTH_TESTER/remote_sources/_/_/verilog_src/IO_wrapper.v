`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:20:52 07/04/2016 
// Design Name: 
// Module Name:    IO_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module IO_BusDelay(iob_in, oser_in,
						fabric_out, iser_out, iob_out);

	
	parameter SIZE = 8;
	// for Spartan 6: DS162 Table 39
	// Max: 255
	parameter NB_TAPS = 0;
	parameter DIRECTION = "INPUT";
	
	localparam DIR = (DIRECTION == "INPUT") ? 1'b1 : 1'b0;
	
	input wire [SIZE-1:0]iob_in; // Coming from an IOB
	input wire [SIZE-1:0]oser_in; // Coming from an OLOGIC2 or an OSERDES2
	
	output wire [SIZE-1:0]fabric_out; // Going to FPGA interco logic
	output wire [SIZE-1:0]iser_out; // Going to an ILOGIC2 or an ISERDES2
	output wire [SIZE-1:0]iob_out; // Going to an IOB
	
	// Generate iodelay2
	
	genvar i;
	generate
		for(i=0; i < SIZE; i = i+1) begin: delays
			IODELAY2 #(
				.IDELAY_VALUE((DIR) ? NB_TAPS : 0),
				.IDELAY2_VALUE(0),
				.IDELAY_MODE("NORMAL"),
				.ODELAY_VALUE((DIR) ? 0 : NB_TAPS),
				.IDELAY_TYPE("FIXED"),
				.DELAY_SRC((DIR) ? "IDATAIN" : "ODATAIN"),
				.SERDES_MODE("NONE"),
				.DATA_RATE("DDR")) 
			delay (
				.IDATAIN(iob_in[i]),
				.ODATAIN(oser_in[i]),
				.T(DIR),
				.DATAOUT(iser_out[i]),
				.DATAOUT2(fabric_out[i]),
				.DOUT(iob_out[i]),
				.CAL(),
				.IOCLK0(),
				.IOCLK1(),
				.CLK(),
				.INC(1'b0),
				.CE(1'b0),
				.RST(),
				.BUSY(),
				.TOUT());
		end
	endgenerate

endmodule

module IO_BusDDR(clk_in_p, clk_in_n, ddr_in, sdr_0_out, sdr_1_out,
						sdr_0_in, sdr_1_in, ddr_out);
	
	parameter SIZE = 8;
	parameter DIRECTION = "INPUT";
	
	localparam DIR = (DIRECTION == "INPUT") ? 1'b1: 1'b0;
	
	input wire clk_in_p;
	input wire clk_in_n;
	input wire [SIZE-1:0]ddr_in; // For DDR -> SDR
	output wire [SIZE-1:0]sdr_0_out;
	output wire [SIZE-1:0]sdr_1_out;
	
	input wire [SIZE-1:0]sdr_0_in; // For SDR -> DDR
	input wire [SIZE-1:0]sdr_1_in;
	output wire [SIZE-1:0]ddr_out;
	
	genvar i;
	generate
		for(i = 0; i < SIZE; i = i + 1) begin: ddrs
			if(DIR) begin
				IDDR2 #(
					// Force align Qx output to C0
					//UG381 Figure 2.6
					.DDR_ALIGNMENT("C1"),
					.SRTYPE("ASYNC"),
					.INIT_Q0(0),
					.INIT_Q1(0))
				ddr (
					.D(ddr_in[i]),
					.C0(clk_in_p),
					.C1(clk_in_n),
					.CE(1'b1),
					.R(1'b0),
					.S(1'b0),
					.Q0(sdr_0_out[i]),
					.Q1(sdr_1_out[i])
				);
			end else begin
				// To write
			end
		end
	endgenerate
	
endmodule
