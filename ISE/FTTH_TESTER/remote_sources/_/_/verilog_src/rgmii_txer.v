`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:18:07 07/04/2016 
// Design Name: 
// Module Name:    rgmii_txer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rgmii_txer(// Physical Interface
						RXC, RXD, RXCTL,
						TXC, TXD, TXCTL,
						
						// FPGA side
						GMII_RXD, GMII_RXDV, GMII_RXER
    );
	 // RGMII PHY Side
    input wire RXC;
    input wire [3:0]RXD;
    input wire RXCTL;
    
	 output reg TXC;
    output reg [3:0]TXD;
    output reg TXCTL;
	 
	 // GMII FPGA Side
	 output wire [7:0]GMII_RXD;
	 output wire GMII_RXDV;
	 output wire GMII_RXER;
	 
	 // RGMII RX Side
	 // RXD => IODelay => IODDR
	 // 39 taps => 2039ps delay on S6 -2 (see ds162 table 39)
	 // 4*8 + 1*7 taps => 4*424ps + 343ps
	 
	 wire [3:0]rxd_delayed;
	 
	 IO_BusDelay #(
		.SIZE(4),
		.NB_TAPS(0),
		.DIRECTION("INPUT"))
	 rx_delay_block (
		.iob_in(RXD),
		.oser_in(),
		.fabric_out(),
		.iser_out(rxd_delayed),
		.iob_out()
	 );
		
	 IO_BusDDR #(
		.SIZE(4),
		.DIRECTION("INPUT"))
	 rx_ddr_block (
		//.ddr_in(rxd_delayed),
		.ddr_in(RXD),
		.clk_in_p(RXC),
		.clk_in_n(~RXC),
		.sdr_0_out(GMII_RXD[7:4]),
		.sdr_1_out(GMII_RXD[3:0])
	 );
	 
	 wire rxctl_delayed;
	 wire rgmii_rxerr;
	 
	 IO_BusDelay #(
		.SIZE(1),
		.NB_TAPS(0),
		.DIRECTION("INPUT"))
	 rxctl_delay_block (
		.iob_in(RXCTL),
		.oser_in(),
		.fabric_out(),
		.iser_out(rxctl_delayed),
		.iob_out()
	 );
	
	 // See RGMII spec section 3.4
	 
	 assign GMII_RXER = GMII_RXDV ^ rgmii_rxerr;
	 
	 IO_BusDDR #(
		.SIZE(1),
		.DIRECTION("INPUT"))
	 rxctl_ddr_block (
		//.ddr_in(rxctl_delayed),
		.ddr_in(RXCTL),
		.clk_in_p(RXC),
		.clk_in_n(~RXC),
		.sdr_0_out(rgmii_rxerr),
		.sdr_1_out(GMII_RXDV)
	 );

endmodule
