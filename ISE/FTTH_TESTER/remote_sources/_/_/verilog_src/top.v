`timescale 1ns / 1ps
`default_nettype none

module top(
    input wire clk50,
	 
    inout wire mdio,
    output wire mdc,
	 
	 output wire nreset,
	 
	 input wire RXC,
	 input wire [3:0]RXD,
	 input wire RXCTL,
	 
	 output wire TXC,
	 output wire [3:0]TXD,
	 output wire TXCTL,
	 
	 inout wire SCL,
	 inout wire SDA,
	 input wire ABS,
	 
	 output wire SFP_TX_DIS,
	 output wire SFP_PW_EN,
	 
	 output wire uart_tx,
	 
	 output wire led_b,
	 output wire led_g,
	 output wire led_r
	 
    );
	 
	 assign mdc = 0;
	 // BUF clk50
	 wire clk50_buf;
	 IBUFG in_clk50(.I(clk50), .O(clk50_buf));

	// Generate the 125MHz
	
	wire raw_pll_out;
	wire clk125;
	
	PLL_BASE #(
		.BANDWIDTH("OPTIMIZED"),
		.CLK_FEEDBACK("CLKOUT0"),
		.CLKFBOUT_MULT(5),
		.CLKOUT0_DIVIDE(2),
		.CLKIN_PERIOD(20.0)
	) pll_125gen (
		.CLKIN(clk50_buf),
		.RST(1'b0),
		.CLKFBIN(clk125),
		
		.CLKFBOUT(),
		
		.CLKOUT0(raw_pll_out),
		.CLKOUT1(),
		.CLKOUT2(),
		.CLKOUT3(),
		.CLKOUT4(),
		.CLKOUT5()
	);
	
	BUFG clk125_bufg(.I(raw_pll_out), .O(clk125));
	
	//General parameters
	localparam SECOND					= 50000000;
	reg [25:0]second_cnt 			= 0;
	
	// Instantiate IP
	
	// ================ I2C  =================== //
	// SFF-8472 Rev 11.0
	
	parameter SFP_A0				= 8'hA0;
	parameter SFP_A2				= 8'hA2;
	
	// A0h Field
	
	
	// A2h Field
	parameter SFP_POW_MSB		= 8'd104;
	parameter SFP_POW_LSB		= 8'd105;
	
	reg [15:0]opt_power = 0;
	reg [15:0]q_opt_power = 0;
	
	/*reg i2c_start = 0;
	reg i2c_read = 0;
	
	wire [7:0]i2c_data_r;
	wire i2c_done;
	wire i2c_nack;
	wire i2c_valid;
	
	simple_i2c_master sfp_i2c(.clk(clk50_buf), .start(i2c_start), .read(i2c_read), .devaddr(SFP_A2[7:1]), .data_wr(8'd0),
										.word_addr(SFP_POW_MSB), .len(8'd2), .data_valid(i2c_valid), .data_rd(i2c_data_r), .done(i2c_done),
										.nack(i2c_nack), .scl(SCL), .sda(SDA));
	
	reg i2c_cnt = 0;
	reg [3:0]i2c_fsm = 0;
	
	localparam I2C_IDLE 				= 0;
	localparam I2C_MSB				= 1;
	localparam I2C_LSB				= 2;
	localparam I2C_LATCH				= 3;
	
	always @(posedge clk50_buf)
	begin
		i2c_start <= 0;
		i2c_read <= 1;
		
		if(i2c_nack) i2c_fsm <= I2C_IDLE;

		case(i2c_fsm)
			I2C_IDLE: begin
				if(second_cnt == SECOND) begin
					i2c_start <= 1;
					i2c_fsm <= I2C_MSB;
				end
			end
			
			I2C_MSB: begin
				if(i2c_valid) begin
					q_opt_power[15:8] <= i2c_data_r;
					i2c_fsm <= I2C_LSB;
				end
			end
			
			I2C_LSB: begin
				if(i2c_valid) begin
					q_opt_power[7:0] <= i2c_data_r;
					i2c_fsm <= I2C_LATCH;
				end
			end
			
			I2C_LATCH: begin
				if(i2c_done) begin
					opt_power <= q_opt_power;
					i2c_fsm <= I2C_IDLE;
				end
			end
		endcase
		
	end*/
	
	// ================ UART =================== //
	reg [7:0]dsend = 8'd52;
	reg u_start = 0;
	
	wire u_done;

	uart_tx #(.BAUD(57600), .CLK_SPEED(25000000))
				uart_debug(.clk(clk50_buf), .start(u_start),
				.dsend(dsend), .bit_out(uart_tx), .done(u_done));

	// ================ MDIO =================== //
	/*reg phy_rd = 0;
	reg phy_wr = 0;
	
	wire mdio_busy;
	
	reg [4:0]reg_addr = 0;
	reg [15:0]data_wr = 0;
	wire [15:0]data_rd;

	MDIO_transceiver mdio_88e1512(.clk(clk50_buf), .mdio(mdio), .mdc(mdc),
							.phy_rd(phy_rd), .phy_wr(phy_wr), .mdio_busy(mdio_busy),
							.reg_addr(reg_addr), .data_wr(data_wr), .data_rd(data_rd));*/
							
	// ================ RGMI =================== //
	
	wire [7:0]gmii_txd;
	wire gmii_txen;
	wire gmii_txer;
	
	rgmii_txer myrgmii(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL),
							.TXC(TXC), .TXD(TXD), .TXCTL(TXCTL),
							
							//GMII_RXD, GMII_RXDV, GMII_RXER,
							
							.GMII_TXC(RXC), //Bridge RX clock to TX Clock
							.GMII_TXD(gmii_txd), .GMII_TXEN(gmii_txen), .GMII_TXER(gmii_txer)
    );
	 
	 tx_arp myarp(.clk(RXC), .GMII_TXD(gmii_txd), .GMII_TXEN(gmii_txen), .GMII_TXER(gmii_txer));
	
	// MDIO FSM
	
	// FSM module control
	/*wire mdio_ready = !phy_wr && !phy_rd && !mdio_busy;
	wire uart_ready = u_done && !u_start;
	reg sync_bit = 0;
	
	reg [1:0]sync_fsm_state = 0;
	
	localparam IDLE					= 0;
	localparam READ_SYNC				= 1;
	localparam SET_PAGE1				= 2;
	localparam CHECK_SYNC			= 3;

	always @(posedge clk50_buf)
	begin
		case(sync_fsm_state)
			IDLE: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE1;
				end
			end
			
			SET_PAGE1: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_wr <= 1'b1;
					reg_addr <= 5'd22;
					data_wr <= 16'd1;
					sync_fsm_state <= READ_SYNC;
				end
			end
			
			READ_SYNC: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_rd <= 1'b1;
					reg_addr <= 5'd17;
					sync_fsm_state <= CHECK_SYNC;
				end
			end
			
			CHECK_SYNC: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE1;
					if(data_rd[5] == 1) begin // We got a 8b/10b sync
						// activate the SFP tx if needed
						sync_bit <= 1;
					end else begin
						// no sync, turn off led and sfp tx
						sync_bit <= 0;
					end
				end
			end
		endcase
	end*/
	
	// The FPGA is flashed
	assign led_r = 1;
	
	// We start the phy
	assign nreset = 1;
	
	// If we have 8b10b sync we light green led and start SFP TX
	assign led_g = 1;//!sync_bit;
	assign SFP_TX_DIS = 0;//!sync_bit;
	
	// If frame detected we light the blue led
	assign led_b = 1;
	
	// We wait for a SFP to be inserted before powering the SFP cage
	assign SFP_PW_EN = !ABS;

endmodule
