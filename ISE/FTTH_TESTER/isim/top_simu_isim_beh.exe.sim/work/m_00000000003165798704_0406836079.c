/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/nats/Desktop/NetworkingFPGA/verilog_src/arp_tester.v";
static int ng1[] = {0, 0};
static int ng2[] = {1, 0};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {125U, 0U};
static unsigned int ng5[] = {0U, 0U};
static unsigned int ng6[] = {85U, 0U};
static int ng7[] = {7, 0};
static unsigned int ng8[] = {213U, 0U};
static unsigned int ng9[] = {64U, 0U};
static int ng10[] = {4, 0};
static int ng11[] = {3, 0};
static const char *ng12 = "arping_crc.data";



static void Cont_36_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;

LAB0:    t1 = (t0 + 4760U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(36, ng0);
    t2 = (t0 + 2000U);
    t3 = *((char **)t2);
    t2 = (t0 + 5672);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 1U;
    t9 = t8;
    t10 = (t3 + 4);
    t11 = *((unsigned int *)t3);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t2, 0, 0);
    t16 = (t0 + 5576);
    *((int *)t16) = 1;

LAB1:    return;
}

static void Always_58_1(char *t0)
{
    char t11[8];
    char t35[8];
    char t36[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int t7;
    char *t8;
    char *t9;
    char *t10;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    char *t43;

LAB0:    t1 = (t0 + 5008U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 5592);
    *((int *)t2) = 1;
    t3 = (t0 + 5040);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(59, ng0);

LAB5:    xsi_set_current_line(61, ng0);
    t4 = ((char*)((ng1)));
    t5 = (t0 + 2880);
    xsi_vlogvar_wait_assign_value(t5, t4, 0, 0, 1, 0LL);
    xsi_set_current_line(62, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    xsi_set_current_line(63, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 3040);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(64, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 3680);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(66, ng0);
    t2 = (t0 + 3520);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);

LAB6:    t5 = (t0 + 744);
    t6 = *((char **)t5);
    t7 = xsi_vlog_unsigned_case_compare(t4, 4, t6, 32);
    if (t7 == 1)
        goto LAB7;

LAB8:    t2 = (t0 + 1152);
    t3 = *((char **)t2);
    t7 = xsi_vlog_unsigned_case_compare(t4, 4, t3, 32);
    if (t7 == 1)
        goto LAB9;

LAB10:    t2 = (t0 + 1288);
    t3 = *((char **)t2);
    t7 = xsi_vlog_unsigned_case_compare(t4, 4, t3, 32);
    if (t7 == 1)
        goto LAB11;

LAB12:    t2 = (t0 + 880);
    t3 = *((char **)t2);
    t7 = xsi_vlog_unsigned_case_compare(t4, 4, t3, 32);
    if (t7 == 1)
        goto LAB13;

LAB14:    t2 = (t0 + 1016);
    t3 = *((char **)t2);
    t7 = xsi_vlog_unsigned_case_compare(t4, 4, t3, 32);
    if (t7 == 1)
        goto LAB15;

LAB16:
LAB17:    goto LAB2;

LAB7:    xsi_set_current_line(67, ng0);

LAB18:    xsi_set_current_line(68, ng0);
    t5 = (t0 + 3360);
    t8 = (t5 + 56U);
    t9 = *((char **)t8);
    t10 = ((char*)((ng3)));
    memset(t11, 0, 8);
    xsi_vlog_unsigned_add(t11, 32, t9, 32, t10, 32);
    t12 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t12, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(69, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng4)));
    memset(t11, 0, 8);
    t8 = (t5 + 4);
    t9 = (t6 + 4);
    t13 = *((unsigned int *)t5);
    t14 = *((unsigned int *)t6);
    t15 = (t13 ^ t14);
    t16 = *((unsigned int *)t8);
    t17 = *((unsigned int *)t9);
    t18 = (t16 ^ t17);
    t19 = (t15 | t18);
    t20 = *((unsigned int *)t8);
    t21 = *((unsigned int *)t9);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB22;

LAB19:    if (t22 != 0)
        goto LAB21;

LAB20:    *((unsigned int *)t11) = 1;

LAB22:    t12 = (t11 + 4);
    t25 = *((unsigned int *)t12);
    t26 = (~(t25));
    t27 = *((unsigned int *)t11);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB23;

LAB24:
LAB25:    goto LAB17;

LAB9:    xsi_set_current_line(75, ng0);

LAB27:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 3360);
    t5 = (t2 + 56U);
    t6 = *((char **)t5);
    t8 = ((char*)((ng3)));
    memset(t11, 0, 8);
    xsi_vlog_unsigned_add(t11, 32, t6, 32, t8, 32);
    t9 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t9, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(77, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 2880);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(78, ng0);
    t2 = ((char*)((ng6)));
    t3 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    xsi_set_current_line(79, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng7)));
    memset(t11, 0, 8);
    t8 = (t5 + 4);
    t9 = (t6 + 4);
    t13 = *((unsigned int *)t5);
    t14 = *((unsigned int *)t6);
    t15 = (t13 ^ t14);
    t16 = *((unsigned int *)t8);
    t17 = *((unsigned int *)t9);
    t18 = (t16 ^ t17);
    t19 = (t15 | t18);
    t20 = *((unsigned int *)t8);
    t21 = *((unsigned int *)t9);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB31;

LAB28:    if (t22 != 0)
        goto LAB30;

LAB29:    *((unsigned int *)t11) = 1;

LAB31:    t12 = (t11 + 4);
    t25 = *((unsigned int *)t12);
    t26 = (~(t25));
    t27 = *((unsigned int *)t11);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB32;

LAB33:
LAB34:    goto LAB17;

LAB11:    xsi_set_current_line(85, ng0);

LAB36:    xsi_set_current_line(86, ng0);
    t2 = ((char*)((ng2)));
    t5 = (t0 + 2880);
    xsi_vlogvar_wait_assign_value(t5, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(87, ng0);
    t2 = ((char*)((ng8)));
    t3 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    xsi_set_current_line(88, ng0);
    t2 = (t0 + 880);
    t3 = *((char **)t2);
    t2 = (t0 + 3520);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 4, 0LL);
    goto LAB17;

LAB13:    xsi_set_current_line(91, ng0);

LAB37:    xsi_set_current_line(92, ng0);
    t2 = ((char*)((ng2)));
    t5 = (t0 + 2880);
    xsi_vlogvar_wait_assign_value(t5, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(93, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 3680);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(94, ng0);
    t2 = (t0 + 3200);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = (t0 + 3200);
    t8 = (t6 + 72U);
    t9 = *((char **)t8);
    t10 = (t0 + 3200);
    t12 = (t10 + 64U);
    t30 = *((char **)t12);
    t31 = (t0 + 3360);
    t32 = (t31 + 56U);
    t33 = *((char **)t32);
    xsi_vlog_generic_get_array_select_value(t11, 8, t5, t9, t30, 2, 1, t33, 32, 2);
    t34 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t34, t11, 0, 0, 8, 0LL);
    xsi_set_current_line(95, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng3)));
    memset(t11, 0, 8);
    xsi_vlog_unsigned_add(t11, 32, t5, 32, t6, 32);
    t8 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t8, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(96, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng9)));
    t8 = ((char*)((ng2)));
    memset(t11, 0, 8);
    xsi_vlog_unsigned_minus(t11, 32, t6, 32, t8, 32);
    t9 = ((char*)((ng10)));
    memset(t35, 0, 8);
    xsi_vlog_unsigned_minus(t35, 32, t11, 32, t9, 32);
    memset(t36, 0, 8);
    t10 = (t5 + 4);
    t12 = (t35 + 4);
    t13 = *((unsigned int *)t5);
    t14 = *((unsigned int *)t35);
    t15 = (t13 ^ t14);
    t16 = *((unsigned int *)t10);
    t17 = *((unsigned int *)t12);
    t18 = (t16 ^ t17);
    t19 = (t15 | t18);
    t20 = *((unsigned int *)t10);
    t21 = *((unsigned int *)t12);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB41;

LAB38:    if (t22 != 0)
        goto LAB40;

LAB39:    *((unsigned int *)t36) = 1;

LAB41:    t31 = (t36 + 4);
    t25 = *((unsigned int *)t31);
    t26 = (~(t25));
    t27 = *((unsigned int *)t36);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB42;

LAB43:
LAB44:    goto LAB17;

LAB15:    xsi_set_current_line(102, ng0);

LAB46:    xsi_set_current_line(103, ng0);
    t2 = ((char*)((ng2)));
    t5 = (t0 + 2880);
    xsi_vlogvar_wait_assign_value(t5, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(104, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng3)));
    memset(t11, 0, 8);
    xsi_vlog_unsigned_add(t11, 32, t5, 32, t6, 32);
    t8 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t8, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(105, ng0);
    t2 = (t0 + 2320U);
    t3 = *((char **)t2);
    memset(t35, 0, 8);
    t2 = (t35 + 4);
    t5 = (t3 + 4);
    t13 = *((unsigned int *)t3);
    t14 = (t13 >> 24);
    *((unsigned int *)t35) = t14;
    t15 = *((unsigned int *)t5);
    t16 = (t15 >> 24);
    *((unsigned int *)t2) = t16;
    t17 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t17 & 255U);
    t18 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t18 & 255U);
    t6 = (t0 + 2320U);
    t8 = *((char **)t6);
    memset(t36, 0, 8);
    t6 = (t36 + 4);
    t9 = (t8 + 4);
    t19 = *((unsigned int *)t8);
    t20 = (t19 >> 0);
    *((unsigned int *)t36) = t20;
    t21 = *((unsigned int *)t9);
    t22 = (t21 >> 0);
    *((unsigned int *)t6) = t22;
    t23 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t23 & 16777215U);
    t24 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t24 & 16777215U);
    xsi_vlogtype_concat(t11, 32, 32, 2U, t36, 24, t35, 8);
    t10 = (t0 + 3840);
    xsi_vlogvar_wait_assign_value(t10, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(106, ng0);
    t2 = (t0 + 2320U);
    t3 = *((char **)t2);
    memset(t11, 0, 8);
    t2 = (t11 + 4);
    t5 = (t3 + 4);
    t13 = *((unsigned int *)t3);
    t14 = (t13 >> 24);
    *((unsigned int *)t11) = t14;
    t15 = *((unsigned int *)t5);
    t16 = (t15 >> 24);
    *((unsigned int *)t2) = t16;
    t17 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t17 & 255U);
    t18 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t18 & 255U);
    t6 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t6, t11, 0, 0, 8, 0LL);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng1)));
    memset(t11, 0, 8);
    t8 = (t5 + 4);
    t9 = (t6 + 4);
    t13 = *((unsigned int *)t5);
    t14 = *((unsigned int *)t6);
    t15 = (t13 ^ t14);
    t16 = *((unsigned int *)t8);
    t17 = *((unsigned int *)t9);
    t18 = (t16 ^ t17);
    t19 = (t15 | t18);
    t20 = *((unsigned int *)t8);
    t21 = *((unsigned int *)t9);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB48;

LAB47:    if (t22 != 0)
        goto LAB49;

LAB50:    t12 = (t11 + 4);
    t25 = *((unsigned int *)t12);
    t26 = (~(t25));
    t27 = *((unsigned int *)t11);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB51;

LAB52:
LAB53:    goto LAB17;

LAB21:    t10 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB22;

LAB23:    xsi_set_current_line(69, ng0);

LAB26:    xsi_set_current_line(70, ng0);
    t30 = (t0 + 1152);
    t31 = *((char **)t30);
    t30 = (t0 + 3520);
    xsi_vlogvar_wait_assign_value(t30, t31, 0, 0, 4, 0LL);
    xsi_set_current_line(71, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 32, 0LL);
    goto LAB25;

LAB30:    t10 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB31;

LAB32:    xsi_set_current_line(79, ng0);

LAB35:    xsi_set_current_line(80, ng0);
    t30 = ((char*)((ng1)));
    t31 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t31, t30, 0, 0, 32, 0LL);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 1288);
    t3 = *((char **)t2);
    t2 = (t0 + 3520);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 4, 0LL);
    goto LAB34;

LAB40:    t30 = (t36 + 4);
    *((unsigned int *)t36) = 1;
    *((unsigned int *)t30) = 1;
    goto LAB41;

LAB42:    xsi_set_current_line(96, ng0);

LAB45:    xsi_set_current_line(97, ng0);
    t32 = (t0 + 1016);
    t33 = *((char **)t32);
    t32 = (t0 + 3520);
    xsi_vlogvar_wait_assign_value(t32, t33, 0, 0, 4, 0LL);
    xsi_set_current_line(98, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 3360);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 32, 0LL);
    goto LAB44;

LAB48:    *((unsigned int *)t11) = 1;
    goto LAB50;

LAB49:    t10 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB50;

LAB51:    xsi_set_current_line(107, ng0);

LAB54:    xsi_set_current_line(108, ng0);
    t30 = (t0 + 3840);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    memset(t35, 0, 8);
    t33 = (t35 + 4);
    t34 = (t32 + 4);
    t37 = *((unsigned int *)t32);
    t38 = (t37 >> 24);
    *((unsigned int *)t35) = t38;
    t39 = *((unsigned int *)t34);
    t40 = (t39 >> 24);
    *((unsigned int *)t33) = t40;
    t41 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t41 & 255U);
    t42 = *((unsigned int *)t33);
    *((unsigned int *)t33) = (t42 & 255U);
    t43 = (t0 + 2720);
    xsi_vlogvar_wait_assign_value(t43, t35, 0, 0, 8, 0LL);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 3840);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    memset(t35, 0, 8);
    t6 = (t35 + 4);
    t8 = (t5 + 4);
    t13 = *((unsigned int *)t5);
    t14 = (t13 >> 24);
    *((unsigned int *)t35) = t14;
    t15 = *((unsigned int *)t8);
    t16 = (t15 >> 24);
    *((unsigned int *)t6) = t16;
    t17 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t17 & 255U);
    t18 = *((unsigned int *)t6);
    *((unsigned int *)t6) = (t18 & 255U);
    t9 = (t0 + 3840);
    t10 = (t9 + 56U);
    t12 = *((char **)t10);
    memset(t36, 0, 8);
    t30 = (t36 + 4);
    t31 = (t12 + 4);
    t19 = *((unsigned int *)t12);
    t20 = (t19 >> 0);
    *((unsigned int *)t36) = t20;
    t21 = *((unsigned int *)t31);
    t22 = (t21 >> 0);
    *((unsigned int *)t30) = t22;
    t23 = *((unsigned int *)t36);
    *((unsigned int *)t36) = (t23 & 16777215U);
    t24 = *((unsigned int *)t30);
    *((unsigned int *)t30) = (t24 & 16777215U);
    xsi_vlogtype_concat(t11, 32, 32, 2U, t36, 24, t35, 8);
    t32 = (t0 + 3840);
    xsi_vlogvar_wait_assign_value(t32, t11, 0, 0, 32, 0LL);
    xsi_set_current_line(110, ng0);
    t2 = (t0 + 3360);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);
    t6 = ((char*)((ng11)));
    memset(t11, 0, 8);
    t8 = (t5 + 4);
    t9 = (t6 + 4);
    t13 = *((unsigned int *)t5);
    t14 = *((unsigned int *)t6);
    t15 = (t13 ^ t14);
    t16 = *((unsigned int *)t8);
    t17 = *((unsigned int *)t9);
    t18 = (t16 ^ t17);
    t19 = (t15 | t18);
    t20 = *((unsigned int *)t8);
    t21 = *((unsigned int *)t9);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB58;

LAB55:    if (t22 != 0)
        goto LAB57;

LAB56:    *((unsigned int *)t11) = 1;

LAB58:    t12 = (t11 + 4);
    t25 = *((unsigned int *)t12);
    t26 = (~(t25));
    t27 = *((unsigned int *)t11);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB59;

LAB60:
LAB61:    goto LAB53;

LAB57:    t10 = (t11 + 4);
    *((unsigned int *)t11) = 1;
    *((unsigned int *)t10) = 1;
    goto LAB58;

LAB59:    xsi_set_current_line(110, ng0);
    t30 = (t0 + 744);
    t31 = *((char **)t30);
    t30 = (t0 + 3520);
    xsi_vlogvar_wait_assign_value(t30, t31, 0, 0, 4, 0LL);
    goto LAB61;

}

static void Initial_116_2(char *t0)
{
    char *t1;

LAB0:    xsi_set_current_line(116, ng0);

LAB2:    xsi_set_current_line(117, ng0);
    t1 = (t0 + 3200);
    xsi_vlogfile_readmemh(ng12, 0, t1, 0, 0, 0, 0);

LAB1:    return;
}


extern void work_m_00000000003165798704_0406836079_init()
{
	static char *pe[] = {(void *)Cont_36_0,(void *)Always_58_1,(void *)Initial_116_2};
	xsi_register_didat("work_m_00000000003165798704_0406836079", "isim/top_simu_isim_beh.exe.sim/work/m_00000000003165798704_0406836079.didat");
	xsi_register_executes(pe);
}
