/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/mkhairy/Desktop/NetworkingFPGA/verilog_src/uart_tx.v";
static int ng1[] = {0, 0};
static int ng2[] = {1, 0};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {0U, 0U};
static int ng5[] = {78, 0};
static int ng6[] = {79, 0};
static int ng7[] = {69, 0};
static int ng8[] = {77, 0};
static int ng9[] = {83, 0};



static void NetDecl_33_0(char *t0)
{
    char t3[8];
    char *t1;
    char *t2;
    char *t4;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;

LAB0:    t1 = (t0 + 5600U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(33, ng0);
    t2 = (t0 + 4200);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    memset(t3, 0, 8);
    t6 = (t5 + 4);
    t7 = *((unsigned int *)t5);
    t8 = *((unsigned int *)t5);
    t8 = (t8 & 1);
    if (*((unsigned int *)t6) != 0)
        goto LAB4;

LAB5:    t10 = 1;

LAB7:    t11 = (t10 <= 7);
    if (t11 == 1)
        goto LAB8;

LAB9:    *((unsigned int *)t3) = t8;

LAB6:    t13 = (t0 + 6528);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memset(t17, 0, 8);
    t18 = 1U;
    t19 = t18;
    t20 = (t3 + 4);
    t21 = *((unsigned int *)t3);
    t18 = (t18 & t21);
    t22 = *((unsigned int *)t20);
    t19 = (t19 & t22);
    t23 = (t17 + 4);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 | t18);
    t25 = *((unsigned int *)t23);
    *((unsigned int *)t23) = (t25 | t19);
    xsi_driver_vfirst_trans(t13, 0, 0U);
    t26 = (t0 + 6416);
    *((int *)t26) = 1;

LAB1:    return;
LAB4:    t9 = (t3 + 4);
    *((unsigned int *)t3) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB6;

LAB8:    t7 = (t7 >> 1);
    t12 = (t7 & 1);
    t8 = (t8 ^ t12);

LAB10:    t10 = (t10 + 1);
    goto LAB7;

}

static void Always_35_1(char *t0)
{
    char t6[8];
    char t8[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    char *t31;

LAB0:    t1 = (t0 + 5848U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(35, ng0);
    t2 = (t0 + 6432);
    *((int *)t2) = 1;
    t3 = (t0 + 5880);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(36, ng0);

LAB5:    xsi_set_current_line(37, ng0);
    t4 = ((char*)((ng1)));
    t5 = (t0 + 3880);
    xsi_vlogvar_wait_assign_value(t5, t4, 0, 0, 1, 0LL);
    xsi_set_current_line(38, ng0);
    t2 = (t0 + 4040);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng2)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 32, t4, 32, t5, 32);
    t7 = (t0 + 4040);
    xsi_vlogvar_wait_assign_value(t7, t6, 0, 0, 32, 0LL);
    xsi_set_current_line(39, ng0);
    t2 = (t0 + 4040);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 1288);
    t7 = *((char **)t5);
    t5 = ((char*)((ng2)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_minus(t6, 32, t7, 32, t5, 32);
    memset(t8, 0, 8);
    t9 = (t4 + 4);
    t10 = (t6 + 4);
    t11 = *((unsigned int *)t4);
    t12 = *((unsigned int *)t6);
    t13 = (t11 ^ t12);
    t14 = *((unsigned int *)t9);
    t15 = *((unsigned int *)t10);
    t16 = (t14 ^ t15);
    t17 = (t13 | t16);
    t18 = *((unsigned int *)t9);
    t19 = *((unsigned int *)t10);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB9;

LAB6:    if (t20 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t8) = 1;

LAB9:    t24 = (t8 + 4);
    t25 = *((unsigned int *)t24);
    t26 = (~(t25));
    t27 = *((unsigned int *)t8);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB10;

LAB11:
LAB12:    goto LAB2;

LAB8:    t23 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(39, ng0);

LAB13:    xsi_set_current_line(40, ng0);
    t30 = ((char*)((ng2)));
    t31 = (t0 + 3880);
    xsi_vlogvar_wait_assign_value(t31, t30, 0, 0, 1, 0LL);
    xsi_set_current_line(41, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 4040);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 32, 0LL);
    goto LAB12;

}

static void Always_57_2(char *t0)
{
    char t6[8];
    char t22[8];
    char t38[8];
    char t54[8];
    char t62[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    char *t30;
    unsigned int t31;
    unsigned int t32;
    unsigned int t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t39;
    char *t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t53;
    char *t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    char *t61;
    unsigned int t63;
    unsigned int t64;
    unsigned int t65;
    char *t66;
    char *t67;
    char *t68;
    unsigned int t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    char *t76;
    char *t77;
    unsigned int t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    unsigned int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    int t86;
    int t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    unsigned int t91;
    unsigned int t92;
    unsigned int t93;
    char *t94;
    unsigned int t95;
    unsigned int t96;
    unsigned int t97;
    unsigned int t98;
    unsigned int t99;
    char *t100;
    char *t101;

LAB0:    t1 = (t0 + 6096U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(57, ng0);
    t2 = (t0 + 6448);
    *((int *)t2) = 1;
    t3 = (t0 + 6128);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(58, ng0);

LAB5:    xsi_set_current_line(59, ng0);
    t4 = (t0 + 2840U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng2)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    memset(t22, 0, 8);
    t23 = (t6 + 4);
    t24 = *((unsigned int *)t23);
    t25 = (~(t24));
    t26 = *((unsigned int *)t6);
    t27 = (t26 & t25);
    t28 = (t27 & 1U);
    if (t28 != 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t23) != 0)
        goto LAB12;

LAB13:    t30 = (t22 + 4);
    t31 = *((unsigned int *)t22);
    t32 = *((unsigned int *)t30);
    t33 = (t31 || t32);
    if (t33 > 0)
        goto LAB14;

LAB15:    memcpy(t62, t22, 8);

LAB16:    t94 = (t62 + 4);
    t95 = *((unsigned int *)t94);
    t96 = (~(t95));
    t97 = *((unsigned int *)t62);
    t98 = (t97 & t96);
    t99 = (t98 != 0);
    if (t99 > 0)
        goto LAB28;

LAB29:
LAB30:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 3880);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (~(t9));
    t11 = *((unsigned int *)t4);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB32;

LAB33:
LAB34:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    *((unsigned int *)t22) = 1;
    goto LAB13;

LAB12:    t29 = (t22 + 4);
    *((unsigned int *)t22) = 1;
    *((unsigned int *)t29) = 1;
    goto LAB13;

LAB14:    t34 = (t0 + 3720);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    t37 = ((char*)((ng2)));
    memset(t38, 0, 8);
    t39 = (t36 + 4);
    t40 = (t37 + 4);
    t41 = *((unsigned int *)t36);
    t42 = *((unsigned int *)t37);
    t43 = (t41 ^ t42);
    t44 = *((unsigned int *)t39);
    t45 = *((unsigned int *)t40);
    t46 = (t44 ^ t45);
    t47 = (t43 | t46);
    t48 = *((unsigned int *)t39);
    t49 = *((unsigned int *)t40);
    t50 = (t48 | t49);
    t51 = (~(t50));
    t52 = (t47 & t51);
    if (t52 != 0)
        goto LAB20;

LAB17:    if (t50 != 0)
        goto LAB19;

LAB18:    *((unsigned int *)t38) = 1;

LAB20:    memset(t54, 0, 8);
    t55 = (t38 + 4);
    t56 = *((unsigned int *)t55);
    t57 = (~(t56));
    t58 = *((unsigned int *)t38);
    t59 = (t58 & t57);
    t60 = (t59 & 1U);
    if (t60 != 0)
        goto LAB21;

LAB22:    if (*((unsigned int *)t55) != 0)
        goto LAB23;

LAB24:    t63 = *((unsigned int *)t22);
    t64 = *((unsigned int *)t54);
    t65 = (t63 & t64);
    *((unsigned int *)t62) = t65;
    t66 = (t22 + 4);
    t67 = (t54 + 4);
    t68 = (t62 + 4);
    t69 = *((unsigned int *)t66);
    t70 = *((unsigned int *)t67);
    t71 = (t69 | t70);
    *((unsigned int *)t68) = t71;
    t72 = *((unsigned int *)t68);
    t73 = (t72 != 0);
    if (t73 == 1)
        goto LAB25;

LAB26:
LAB27:    goto LAB16;

LAB19:    t53 = (t38 + 4);
    *((unsigned int *)t38) = 1;
    *((unsigned int *)t53) = 1;
    goto LAB20;

LAB21:    *((unsigned int *)t54) = 1;
    goto LAB24;

LAB23:    t61 = (t54 + 4);
    *((unsigned int *)t54) = 1;
    *((unsigned int *)t61) = 1;
    goto LAB24;

LAB25:    t74 = *((unsigned int *)t62);
    t75 = *((unsigned int *)t68);
    *((unsigned int *)t62) = (t74 | t75);
    t76 = (t22 + 4);
    t77 = (t54 + 4);
    t78 = *((unsigned int *)t22);
    t79 = (~(t78));
    t80 = *((unsigned int *)t76);
    t81 = (~(t80));
    t82 = *((unsigned int *)t54);
    t83 = (~(t82));
    t84 = *((unsigned int *)t77);
    t85 = (~(t84));
    t86 = (t79 & t81);
    t87 = (t83 & t85);
    t88 = (~(t86));
    t89 = (~(t87));
    t90 = *((unsigned int *)t68);
    *((unsigned int *)t68) = (t90 & t88);
    t91 = *((unsigned int *)t68);
    *((unsigned int *)t68) = (t91 & t89);
    t92 = *((unsigned int *)t62);
    *((unsigned int *)t62) = (t92 & t88);
    t93 = *((unsigned int *)t62);
    *((unsigned int *)t62) = (t93 & t89);
    goto LAB27;

LAB28:    xsi_set_current_line(59, ng0);

LAB31:    xsi_set_current_line(60, ng0);
    t100 = ((char*)((ng3)));
    t101 = (t0 + 4680);
    xsi_vlogvar_wait_assign_value(t101, t100, 0, 0, 1, 0LL);
    xsi_set_current_line(61, ng0);
    t2 = (t0 + 3000U);
    t3 = *((char **)t2);
    t2 = (t0 + 4200);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 8, 0LL);
    xsi_set_current_line(62, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 3720);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    goto LAB30;

LAB32:    xsi_set_current_line(64, ng0);

LAB35:    xsi_set_current_line(65, ng0);
    t7 = (t0 + 4360);
    t8 = (t7 + 56U);
    t21 = *((char **)t8);

LAB36:    t23 = (t0 + 1424);
    t29 = *((char **)t23);
    t86 = xsi_vlog_unsigned_case_compare(t21, 3, t29, 32);
    if (t86 == 1)
        goto LAB37;

LAB38:    t2 = (t0 + 1560);
    t3 = *((char **)t2);
    t86 = xsi_vlog_unsigned_case_compare(t21, 3, t3, 32);
    if (t86 == 1)
        goto LAB39;

LAB40:    t2 = (t0 + 1696);
    t3 = *((char **)t2);
    t86 = xsi_vlog_unsigned_case_compare(t21, 3, t3, 32);
    if (t86 == 1)
        goto LAB41;

LAB42:    t2 = (t0 + 1832);
    t3 = *((char **)t2);
    t86 = xsi_vlog_unsigned_case_compare(t21, 3, t3, 32);
    if (t86 == 1)
        goto LAB43;

LAB44:    t2 = (t0 + 1968);
    t3 = *((char **)t2);
    t86 = xsi_vlog_unsigned_case_compare(t21, 3, t3, 32);
    if (t86 == 1)
        goto LAB45;

LAB46:
LAB48:
LAB47:    xsi_set_current_line(110, ng0);
    t2 = (t0 + 1424);
    t3 = *((char **)t2);
    t2 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);

LAB49:    goto LAB34;

LAB37:    xsi_set_current_line(66, ng0);

LAB50:    xsi_set_current_line(67, ng0);
    t23 = ((char*)((ng4)));
    t30 = (t0 + 4520);
    xsi_vlogvar_wait_assign_value(t30, t23, 0, 0, 8, 0LL);
    xsi_set_current_line(68, ng0);
    t2 = (t0 + 4680);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = (~(t9));
    t11 = *((unsigned int *)t4);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB51;

LAB52:
LAB53:    goto LAB49;

LAB39:    xsi_set_current_line(72, ng0);

LAB55:    xsi_set_current_line(73, ng0);
    t2 = ((char*)((ng4)));
    t4 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t4, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 1696);
    t3 = *((char **)t2);
    t2 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    goto LAB49;

LAB41:    xsi_set_current_line(76, ng0);

LAB56:    xsi_set_current_line(77, ng0);
    t2 = (t0 + 4520);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t7 = (t0 + 880);
    t8 = *((char **)t7);
    t7 = ((char*)((ng2)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_minus(t6, 32, t8, 32, t7, 32);
    memset(t22, 0, 8);
    t23 = (t5 + 4);
    if (*((unsigned int *)t23) != 0)
        goto LAB58;

LAB57:    t29 = (t6 + 4);
    if (*((unsigned int *)t29) != 0)
        goto LAB58;

LAB61:    if (*((unsigned int *)t5) < *((unsigned int *)t6))
        goto LAB60;

LAB59:    *((unsigned int *)t22) = 1;

LAB60:    t34 = (t22 + 4);
    t9 = *((unsigned int *)t34);
    t10 = (~(t9));
    t11 = *((unsigned int *)t22);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB62;

LAB63:    xsi_set_current_line(83, ng0);

LAB69:    xsi_set_current_line(84, ng0);
    t2 = (t0 + 4520);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 8, t4, 8, t5, 8);
    t7 = (t0 + 4520);
    xsi_vlogvar_wait_assign_value(t7, t6, 0, 0, 8, 0LL);

LAB64:    xsi_set_current_line(86, ng0);
    t2 = (t0 + 4200);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 4200);
    t7 = (t5 + 72U);
    t8 = *((char **)t7);
    t23 = (t0 + 4520);
    t29 = (t23 + 56U);
    t30 = *((char **)t29);
    xsi_vlog_generic_get_index_select_value(t6, 1, t4, t8, 2, t30, 8, 2);
    t34 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t34, t6, 0, 0, 1, 0LL);
    goto LAB49;

LAB43:    xsi_set_current_line(88, ng0);

LAB70:    xsi_set_current_line(89, ng0);
    t2 = ((char*)((ng5)));
    t4 = ((char*)((ng6)));
    memset(t6, 0, 8);
    t5 = (t2 + 4);
    t7 = (t4 + 4);
    t9 = *((unsigned int *)t2);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t5);
    t13 = *((unsigned int *)t7);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t5);
    t17 = *((unsigned int *)t7);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB74;

LAB71:    if (t18 != 0)
        goto LAB73;

LAB72:    *((unsigned int *)t6) = 1;

LAB74:    t23 = (t6 + 4);
    t24 = *((unsigned int *)t23);
    t25 = (~(t24));
    t26 = *((unsigned int *)t6);
    t27 = (t26 & t25);
    t28 = (t27 != 0);
    if (t28 > 0)
        goto LAB75;

LAB76:    xsi_set_current_line(91, ng0);
    t2 = ((char*)((ng5)));
    t3 = ((char*)((ng7)));
    memset(t6, 0, 8);
    t4 = (t2 + 4);
    t5 = (t3 + 4);
    t9 = *((unsigned int *)t2);
    t10 = *((unsigned int *)t3);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t5);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t4);
    t17 = *((unsigned int *)t5);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB87;

LAB84:    if (t18 != 0)
        goto LAB86;

LAB85:    *((unsigned int *)t6) = 1;

LAB87:    t8 = (t6 + 4);
    t24 = *((unsigned int *)t8);
    t25 = (~(t24));
    t26 = *((unsigned int *)t6);
    t27 = (t26 & t25);
    t28 = (t27 != 0);
    if (t28 > 0)
        goto LAB88;

LAB89:    xsi_set_current_line(93, ng0);
    t2 = ((char*)((ng5)));
    t3 = ((char*)((ng8)));
    memset(t6, 0, 8);
    t4 = (t2 + 4);
    t5 = (t3 + 4);
    t9 = *((unsigned int *)t2);
    t10 = *((unsigned int *)t3);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t5);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t4);
    t17 = *((unsigned int *)t5);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB94;

LAB91:    if (t18 != 0)
        goto LAB93;

LAB92:    *((unsigned int *)t6) = 1;

LAB94:    t8 = (t6 + 4);
    t24 = *((unsigned int *)t8);
    t25 = (~(t24));
    t26 = *((unsigned int *)t6);
    t27 = (t26 & t25);
    t28 = (t27 != 0);
    if (t28 > 0)
        goto LAB95;

LAB96:    xsi_set_current_line(95, ng0);
    t2 = ((char*)((ng5)));
    t3 = ((char*)((ng9)));
    memset(t6, 0, 8);
    t4 = (t2 + 4);
    t5 = (t3 + 4);
    t9 = *((unsigned int *)t2);
    t10 = *((unsigned int *)t3);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t4);
    t13 = *((unsigned int *)t5);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t4);
    t17 = *((unsigned int *)t5);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB101;

LAB98:    if (t18 != 0)
        goto LAB100;

LAB99:    *((unsigned int *)t6) = 1;

LAB101:    t8 = (t6 + 4);
    t24 = *((unsigned int *)t8);
    t25 = (~(t24));
    t26 = *((unsigned int *)t6);
    t27 = (t26 & t25);
    t28 = (t27 != 0);
    if (t28 > 0)
        goto LAB102;

LAB103:
LAB104:
LAB97:
LAB90:
LAB77:    xsi_set_current_line(97, ng0);
    t2 = (t0 + 1968);
    t3 = *((char **)t2);
    t2 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(98, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 4520);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    goto LAB49;

LAB45:    xsi_set_current_line(100, ng0);

LAB105:    xsi_set_current_line(101, ng0);
    t2 = (t0 + 4520);
    t4 = (t2 + 56U);
    t5 = *((char **)t4);
    t7 = (t0 + 744);
    t8 = *((char **)t7);
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB107;

LAB106:    t23 = (t8 + 4);
    if (*((unsigned int *)t23) != 0)
        goto LAB107;

LAB110:    if (*((unsigned int *)t5) < *((unsigned int *)t8))
        goto LAB108;

LAB109:    t30 = (t6 + 4);
    t9 = *((unsigned int *)t30);
    t10 = (~(t9));
    t11 = *((unsigned int *)t6);
    t12 = (t11 & t10);
    t13 = (t12 != 0);
    if (t13 > 0)
        goto LAB111;

LAB112:    xsi_set_current_line(104, ng0);

LAB115:    xsi_set_current_line(105, ng0);
    t2 = (t0 + 1424);
    t3 = *((char **)t2);
    t2 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);
    xsi_set_current_line(106, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 3720);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);
    xsi_set_current_line(107, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 4680);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 1, 0LL);

LAB113:    goto LAB49;

LAB51:    xsi_set_current_line(68, ng0);

LAB54:    xsi_set_current_line(69, ng0);
    t7 = (t0 + 1560);
    t8 = *((char **)t7);
    t7 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t7, t8, 0, 0, 3, 0LL);
    goto LAB53;

LAB58:    t30 = (t22 + 4);
    *((unsigned int *)t22) = 1;
    *((unsigned int *)t30) = 1;
    goto LAB60;

LAB62:    xsi_set_current_line(77, ng0);

LAB65:    xsi_set_current_line(78, ng0);
    t35 = (t0 + 1152);
    t36 = *((char **)t35);
    t35 = (t36 + 4);
    t14 = *((unsigned int *)t35);
    t15 = (~(t14));
    t16 = *((unsigned int *)t36);
    t17 = (t16 & t15);
    t18 = (t17 != 0);
    if (t18 > 0)
        goto LAB66;

LAB67:    xsi_set_current_line(81, ng0);
    t2 = (t0 + 1968);
    t3 = *((char **)t2);
    t2 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t2, t3, 0, 0, 3, 0LL);

LAB68:    xsi_set_current_line(82, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 4520);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    goto LAB64;

LAB66:    xsi_set_current_line(79, ng0);
    t37 = (t0 + 1832);
    t39 = *((char **)t37);
    t37 = (t0 + 4360);
    xsi_vlogvar_wait_assign_value(t37, t39, 0, 0, 3, 0LL);
    goto LAB68;

LAB73:    t8 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t8) = 1;
    goto LAB74;

LAB75:    xsi_set_current_line(90, ng0);
    t29 = (t0 + 3160U);
    t30 = *((char **)t29);
    memset(t22, 0, 8);
    t29 = (t30 + 4);
    t31 = *((unsigned int *)t29);
    t32 = (~(t31));
    t33 = *((unsigned int *)t30);
    t41 = (t33 & t32);
    t42 = (t41 & 1U);
    if (t42 != 0)
        goto LAB81;

LAB79:    if (*((unsigned int *)t29) == 0)
        goto LAB78;

LAB80:    t34 = (t22 + 4);
    *((unsigned int *)t22) = 1;
    *((unsigned int *)t34) = 1;

LAB81:    t35 = (t22 + 4);
    t36 = (t30 + 4);
    t43 = *((unsigned int *)t30);
    t44 = (~(t43));
    *((unsigned int *)t22) = t44;
    *((unsigned int *)t35) = 0;
    if (*((unsigned int *)t36) != 0)
        goto LAB83;

LAB82:    t49 = *((unsigned int *)t22);
    *((unsigned int *)t22) = (t49 & 1U);
    t50 = *((unsigned int *)t35);
    *((unsigned int *)t35) = (t50 & 1U);
    t37 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t37, t22, 0, 0, 1, 0LL);
    goto LAB77;

LAB78:    *((unsigned int *)t22) = 1;
    goto LAB81;

LAB83:    t45 = *((unsigned int *)t22);
    t46 = *((unsigned int *)t36);
    *((unsigned int *)t22) = (t45 | t46);
    t47 = *((unsigned int *)t35);
    t48 = *((unsigned int *)t36);
    *((unsigned int *)t35) = (t47 | t48);
    goto LAB82;

LAB86:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB87;

LAB88:    xsi_set_current_line(92, ng0);
    t23 = (t0 + 3160U);
    t29 = *((char **)t23);
    t23 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t23, t29, 0, 0, 1, 0LL);
    goto LAB90;

LAB93:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB94;

LAB95:    xsi_set_current_line(94, ng0);
    t23 = ((char*)((ng3)));
    t29 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t29, t23, 0, 0, 1, 0LL);
    goto LAB97;

LAB100:    t7 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t7) = 1;
    goto LAB101;

LAB102:    xsi_set_current_line(96, ng0);
    t23 = ((char*)((ng4)));
    t29 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t29, t23, 0, 0, 1, 0LL);
    goto LAB104;

LAB107:    t29 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t29) = 1;
    goto LAB109;

LAB108:    *((unsigned int *)t6) = 1;
    goto LAB109;

LAB111:    xsi_set_current_line(101, ng0);

LAB114:    xsi_set_current_line(102, ng0);
    t34 = ((char*)((ng3)));
    t35 = (t0 + 3560);
    xsi_vlogvar_wait_assign_value(t35, t34, 0, 0, 1, 0LL);
    xsi_set_current_line(103, ng0);
    t2 = (t0 + 4520);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t6, 0, 8);
    xsi_vlog_unsigned_add(t6, 8, t4, 8, t5, 8);
    t7 = (t0 + 4520);
    xsi_vlogvar_wait_assign_value(t7, t6, 0, 0, 8, 0LL);
    goto LAB113;

}


extern void work_m_00000000003862867274_2569726710_init()
{
	static char *pe[] = {(void *)NetDecl_33_0,(void *)Always_35_1,(void *)Always_57_2};
	xsi_register_didat("work_m_00000000003862867274_2569726710", "isim/top_simu_isim_beh.exe.sim/work/m_00000000003862867274_2569726710.didat");
	xsi_register_executes(pe);
}
