/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    unisims_ver_m_00000000001946988858_2297623829_init();
    unisims_ver_m_00000000004091665089_2069738482_init();
    unisims_ver_m_00000000003084551676_0883485652_init();
    unisims_ver_m_00000000003266096158_2593380106_init();
    work_m_00000000003862867274_2569726710_init();
    unisims_ver_m_00000000000888323161_4005324139_init();
    work_m_00000000001400562873_0128367464_init();
    unisims_ver_m_00000000002527561478_3247546729_init();
    work_m_00000000000170544358_0881180064_init();
    work_m_00000000002128459410_2060537645_init();
    work_m_00000000000468980050_1240615397_init();
    unisims_ver_m_00000000002304772977_3878188151_init();
    work_m_00000000002422203037_1386396593_init();
    unisims_ver_m_00000000000888323161_1113947916_init();
    work_m_00000000002128459410_2709752158_init();
    work_m_00000000000985433618_3749788820_init();
    work_m_00000000000492015648_0098177540_init();
    work_m_00000000000627120934_0208763007_init();
    work_m_00000000000818671749_0406836079_init();
    work_m_00000000001813080822_3823007873_init();
    work_m_00000000003091793636_4191191692_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000003091793636_4191191692");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
