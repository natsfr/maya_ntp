
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name MDIO_TEST -dir "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/planAhead_run_3" -part xc6slx9tqg144-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST} }
set_property target_constrs_file "top.ucf" [current_fileset -constrset]
add_files [list {top.ucf}] -fileset [get_property constrset [current_run]]
link_design
