
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name MDIO_TEST -dir "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/planAhead_run_1" -part xc6slx9tqg144-2
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST} }
set_property target_constrs_file "top.ucf" [current_fileset -constrset]
add_files [list {top.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/top.ncd"
if {[catch {read_twx -name results_1 -file "E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/top.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"E:/Projets_Cadence/FTTH_Tester/HDL/ISE/MDIO_TEST/top.twx\": $eInfo"
}
