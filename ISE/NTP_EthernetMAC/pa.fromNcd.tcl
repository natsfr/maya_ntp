
# PlanAhead Launch Script for Post PAR Floorplanning, created by Project Navigator

create_project -name NTP_EthernetMAC -dir "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/planAhead_run_2" -part xc6slx9tqg144-3
set srcset [get_property srcset [current_run -impl]]
set_property design_mode GateLvl $srcset
set_property edif_top_file "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/top_frame_proc.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC} }
set_property target_constrs_file "C:/Users/nats/Desktop/NetworkingFPGA/verilog_src/top.ucf" [current_fileset -constrset]
add_files [list {C:/Users/nats/Desktop/NetworkingFPGA/verilog_src/top.ucf}] -fileset [get_property constrset [current_run]]
link_design
read_xdl -file "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/top_frame_proc.ncd"
if {[catch {read_twx -name results_1 -file "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/top_frame_proc.twx"} eInfo]} {
   puts "WARNING: there was a problem importing \"C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/top_frame_proc.twx\": $eInfo"
}
