
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name NTP_EthernetMAC -dir "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/planAhead_run_2" -part xc6slx9tqg144-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC/top_frame_proc.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {C:/Users/nats/Desktop/NetworkingFPGA/ISE/NTP_EthernetMAC} }
set_property target_constrs_file "C:/Users/nats/Desktop/NetworkingFPGA/verilog_src/top.ucf" [current_fileset -constrset]
add_files [list {C:/Users/nats/Desktop/NetworkingFPGA/verilog_src/top.ucf}] -fileset [get_property constrset [current_run]]
link_design
