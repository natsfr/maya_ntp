`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    00:11:52 12/31/2016 
// Design Name: 
// Module Name:    tb_simu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module tb_simu(
    );
	
	reg RXC = 0;
	always #2 RXC <= ~RXC;
    
    reg RXCd = 0;
    
    always @(posedge RXC or negedge RXC)
    begin
        RXCd <= #1 RXC;
    end
    
	reg [7:0]cnt = 0;
	
	reg RXCTL = 0;
	reg [3:0]RXD = 0;
	
	always @(posedge RXCd or negedge RXCd)
	begin
		if(cnt == 1) begin
			RXD <= 4'b0101;
		end else if(cnt == 2)
			RXD <= 4'b1101;
        else
            RXD <= RXD + 1;
		cnt <= cnt + 1;
        if(cnt == 120) begin
            cnt <= 0;
        end
	end
    
    always @(negedge RXCd) begin
        if(cnt == 1) RXCTL <= 1;
        if(cnt == 120) RXCTL <= 0;
    end
	
	wire TXC;
	wire TXCTL;
	wire [3:0]TXD;
	
	top_frame_proc fpga(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL), .TXC(TXC), .TXD(TXD), .TXCTL(TXCTL));

endmodule
