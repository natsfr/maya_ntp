#ChipScope Core Generator Project File Version 3.0
#Wed Jan 11 14:05:57 Paris, Madrid 2017
SignalExport.clockChannel=CLK
SignalExport.dataChannel<0000>=DATA[0]
SignalExport.dataChannel<0001>=DATA[1]
SignalExport.dataChannel<0002>=DATA[2]
SignalExport.dataChannel<0003>=DATA[3]
SignalExport.dataChannel<0004>=DATA[4]
SignalExport.dataChannel<0005>=DATA[5]
SignalExport.dataChannel<0006>=DATA[6]
SignalExport.dataChannel<0007>=DATA[7]
SignalExport.dataEqualsTrigger=false
SignalExport.dataPortWidth=8
SignalExport.triggerChannel<0000><0000>=TRIG0[0]
SignalExport.triggerChannel<0001><0000>=TRIG1[0]
SignalExport.triggerChannel<0001><0001>=TRIG1[1]
SignalExport.triggerChannel<0001><0002>=TRIG1[2]
SignalExport.triggerChannel<0001><0003>=TRIG1[3]
SignalExport.triggerChannel<0001><0004>=TRIG1[4]
SignalExport.triggerChannel<0001><0005>=TRIG1[5]
SignalExport.triggerChannel<0001><0006>=TRIG1[6]
SignalExport.triggerChannel<0001><0007>=TRIG1[7]
SignalExport.triggerPort<0000>.name=TRIG0
SignalExport.triggerPort<0001>.name=TRIG1
SignalExport.triggerPortCount=2
SignalExport.triggerPortIsData<0000>=false
SignalExport.triggerPortIsData<0001>=false
SignalExport.triggerPortWidth<0000>=1
SignalExport.triggerPortWidth<0001>=8
SignalExport.type=ila

