`timescale 1ns / 1ps
`default_nettype none
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:18:35 07/06/2016 
// Design Name: 
// Module Name:    frame_filter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module tx_arp(
    clk,
    
    GMII_TXC,
    GMII_TXD, GMII_TXEN, GMII_TXER);
    
    input wire clk;
    
    output wire GMII_TXC;
    output reg [7:0]GMII_TXD = 0;
    output reg GMII_TXEN = 0;
    output reg GMII_TXER = 0;
    
    assign GMII_TXC = clk;
    
    parameter SECOND = 32'd125000000;
	 parameter FRAME_LEN = 32'd64;
    
    reg [7:0]frame [0:FRAME_LEN-1];
    
    reg [31:0]cnt = 32'd0;
    reg [3:0]fsm_state = 4'd0;
	 
	 reg crc_rst = 1;
	 wire [31:0]crc_w;
	 reg [31:0]crc_r = 0;
	 
	 reg [7:0]tx_data = 0;
	 reg [7:0]tx_data_d = 0;
	 
	 eth_crc_8_no_dreverse mycrc(.clk(clk), .data(tx_data), .rst(crc_rst), .crc(crc_w));
    
    localparam TX_IDLE                  = 0;
    localparam SEND_DATA                = 1;
	 localparam SEND_CRC						 = 2;
	 localparam SEND_PREAMBLE				 = 3;
	 localparam SEND_SFD						 = 4;
    
    always @(posedge clk)
    begin
        
        GMII_TXEN <= 0;
        GMII_TXD <= 0;
        GMII_TXER <= 0;
		  crc_rst <= 1;
        
        case(fsm_state)
            TX_IDLE: begin
                cnt <= cnt + 32'd1;
                if(cnt == 32'd125000000) begin
                    fsm_state <= SEND_PREAMBLE;
                    cnt <= 32'd0;
                end
            end
				
				SEND_PREAMBLE: begin
					cnt <= cnt + 32'd1;
					GMII_TXEN <= 1;
					GMII_TXD <= 8'h55;
					if(cnt == 7) begin
						cnt <= 0;
						fsm_state <= SEND_SFD;
					end
				end
				
				SEND_SFD: begin
					GMII_TXEN <= 1;
					GMII_TXD <= 8'hD5;
					fsm_state <= SEND_DATA;
					tx_data <= frame[0]; // 1 cycle before for crc !
					crc_rst <= 0;
					cnt <= 1;
				end
            
            SEND_DATA: begin
                GMII_TXEN <= 1;
					 crc_rst <= 0;
                tx_data <= frame[cnt];
					 GMII_TXD <= tx_data;
                cnt <= cnt + 32'd1;
                if(cnt == FRAME_LEN-4) begin
                    fsm_state <= SEND_CRC;
                    cnt <= 0;
                end
            end
				
				SEND_CRC: begin
					GMII_TXEN <= 1;
					cnt <= cnt + 32'd1;
					crc_r <= {crc_w[23:0], crc_w[31:24]};
					GMII_TXD <= crc_w[31:24];
					if(cnt != 0) begin
						GMII_TXD <= crc_r[31:24];
						crc_r <= {crc_r[23:0], crc_r[31:24]};
						if(cnt ==3) fsm_state <= TX_IDLE;
					end
				end
        endcase
    end
    
    initial begin
        $readmemh("arping_crc.data", frame);
    end

endmodule