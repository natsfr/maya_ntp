module tb_packet_fifo();

    reg clk = 1;
    always #1 clk <= ~clk;
    
    reg [15:0]data_cnt = 0;
    reg [7:0]data_crap = 0;
    
    reg fifo_wr = 0;
    reg fifo_dv = 0;
    reg fifo_dc = 0;
    
    always @(posedge clk)
    begin
        fifo_wr <= 0;
        fifo_dc <= 0;
        fifo_dv <= 0;
        data_crap <= 8'hA0;
        
        data_cnt <= data_cnt + 1;
        
        if(data_cnt >= 1 && data_cnt < 10) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 10) begin
            fifo_wr <= 0;
            fifo_dv <= 1;
        end else if (data_cnt >= 11 && data_cnt < 35) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 35) begin
            fifo_wr <= 0;
            fifo_dc <= 1;
        end else if (data_cnt >= 36 && data_cnt < 128) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 128) begin
            fifo_wr <= 0;
            fifo_dv <= 1;
        end else if (data_cnt >= 129 && data_cnt < 150) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 150) begin
            fifo_wr <= 0;
            fifo_dv <= 1;
        end else if (data_cnt >= 151 && data_cnt < 20000) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 20000) begin
            fifo_wr <= 0;
            fifo_dv <= 1;
        end
    end
    
    reg fifo_rd = 0;
    
    wire fifo_done;
    wire [7:0]fifo_data;
    wire fifo_empty;
    wire fifo_full;
    
    reg [7:0]rback[0:256];
    reg [7:0]read_cnt = 0;
    reg [7:0]rbd = 0;
    
    always @(posedge clk)
    begin
        fifo_rd <= 0;
        rbd <= 0;
        if(!fifo_empty && !fifo_done) begin
            fifo_rd <= 1;
        end
        if(fifo_rd) begin
            rback[read_cnt] <= fifo_data;
            rbd <= fifo_data;
            read_cnt <= read_cnt + 1;
        end
    end
    
    packet_fifo fifo_test(.clk(clk), .data_wr(fifo_wr), .data_in(data_crap),
                        .data_valid(fifo_dv), .data_cancel(fifo_dc), 
                        .data_rd(fifo_rd), .packet_done(fifo_done), .data_out(fifo_data),
                        .fifo_empty(fifo_empty), .fifo_full(fifo_full));

    initial begin
        $dumpfile("parsim.vcd");
        $dumpvars(0, tb_packet_fifo);
        #80000
        $finish;
    end
                        
endmodule
