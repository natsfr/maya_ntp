module tb_packet_fifo();

    reg clk = 1;
    always #1 clk <= ~clk;
    
    reg [15:0]data_cnt = 0;
    reg [7:0]data_crap = 0;
    
    reg fifo_wr = 0;
    reg fifo_dv = 0;
    reg fifo_dc = 0;
    
    
    always @(posedge clk)
    begin
        fifo_wr <= 0;
        fifo_dc <= 0;
        fifo_dw <= 0;
        data_crap <= 0;
        
        data_cnt <= data_cnt + 1;
        
        if(data_cnt == 1) begin
            fifo_wr <= 1;
            data_crap <= data_crap + 1;
        end else if (data_cnt == 10) begin
            fifo_wr <= 0;
            fifo_dv <= 1;
        end
    end
    
    packet_fifo fifo_test(.clk(clk), .data_wr(fifo_wr), .data_in(data_crap),
                        .data_valid(fifo_dv), .data_cancel(fifo_dc));

endmodule
