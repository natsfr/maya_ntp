module packet_fifo(
    input wire clk,
    
    // Write part
    input wire data_wr,
    input wire [7:0]data_in,
    input wire data_valid,
    input wire data_cancel,
    
    // Read part
    input wire data_rd,
    
    output wire packet_done,
    output reg [7:0]data_out = 0,
    
    output wire fifo_empty,
    output wire fifo_full
    );
    
    parameter FIFO_SIZE = 16'd16384;
    parameter LEN_FIFO_SIZE = 8'd32;
    
    parameter TRIM_END = 0; // Used to remove the end of packet (CRC)
    
    parameter FIFO_BIT = 14;
    parameter FIFO_LEN_BIT = 5;
    
    reg [7:0]fifo[0:FIFO_SIZE-1];
    reg [15:0]fifo_len[0:LEN_FIFO_SIZE-1];
    
    // We use MSB to know if we wrapped
    reg [FIFO_BIT:0]fifo_wr_p = 0;
    reg [FIFO_BIT:0]fifo_tmp_wr_p = 0;
    
    reg [15:0]wr_len_cnt = 0;
    reg [FIFO_LEN_BIT-1:0]fifo_len_wr_p = 0;
    
    always @(posedge clk)
    begin : WRITE_P
        if(data_wr) begin
            if(!fifo_full) begin
                fifo[fifo_tmp_wr_p[FIFO_BIT-1:0]] <= data_in; // Write to current location
                fifo_tmp_wr_p <= fifo_tmp_wr_p + 1;
                wr_len_cnt <= wr_len_cnt + 1;
            end
        end else if(data_valid) begin
            fifo_wr_p <= fifo_tmp_wr_p - TRIM_END;
            fifo_tmp_wr_p <= fifo_tmp_wr_p - TRIM_END;
            fifo_len[fifo_len_wr_p] <= wr_len_cnt - TRIM_END;
            fifo_len_wr_p <= fifo_len_wr_p + 1;
            wr_len_cnt <= 0;
        end else if(data_cancel) begin
            fifo_tmp_wr_p <= fifo_wr_p;
            wr_len_cnt <= 0;
        end
    end
    
    reg [FIFO_BIT:0]fifo_rd_p = 0;
    reg [FIFO_LEN_BIT-1:0]fifo_len_rd_p = 0;
    
    reg [15:0]rd_len_cnt = 0;
    reg [15:0]packet_len = 0;
    
    reg first_byte = 1;
    
    always @(posedge clk)
    begin : READ_P
        rd_len_cnt <= 0;
        first_byte <= 1;
        
        if(data_rd && !fifo_empty) begin
            if(first_byte) begin
                packet_len <= fifo_len[fifo_len_rd_p];
                fifo_len_rd_p <= fifo_len_rd_p + 1;
            end
            
            first_byte <= 0;
            
            rd_len_cnt <= rd_len_cnt + 1;
            
            data_out <= fifo[fifo_rd_p[FIFO_BIT-1:0]];
            fifo_rd_p <= fifo_rd_p + 1;
        end
    end
    
    // Packet done
    assign packet_done = (rd_len_cnt == packet_len) && !first_byte;
    
    // Read pointer behind Write pointer
    // Problem need to use tmp pointer too
    wire empty_bit = (fifo_rd_p[FIFO_BIT] == fifo_wr_p[FIFO_BIT]);
    wire equal_empty_bit = (fifo_rd_p[FIFO_BIT-1:0] == fifo_wr_p[FIFO_BIT-1:0]);
    
    wire full_bit = (fifo_rd_p[FIFO_BIT] != fifo_tmp_wr_p[FIFO_BIT]);
    wire equal_full_bit = (fifo_rd_p[FIFO_BIT-1:0] == fifo_tmp_wr_p[FIFO_BIT-1:0]);
    
    assign fifo_empty = equal_empty_bit & empty_bit;
    assign fifo_full = equal_full_bit & full_bit;
    
endmodule
