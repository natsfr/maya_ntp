module simple_i2c_master(clk, start, read, devaddr, data_wr, word_addr, len,
                         data_valid, data_rd, done, nack, scl, sda);
    
    parameter CLK_SPEED = 50000000; // Hz
    parameter I2C_SPEED = 1000; // Hz
    localparam CNT_CLK = (CLK_SPEED / I2C_SPEED) / 2;
	localparam CLK_DELAY = CNT_CLK/2;
    
    input wire clk;
    input wire start;
    input wire read;
    input wire [6:0]devaddr;
    input wire [7:0]data_wr;
	input wire [7:0]word_addr;
	input wire [7:0]len;
    
	output reg data_valid = 0;
    output reg [7:0]data_rd = 0;
    output reg done = 0;
    output reg nack = 0;
    
    inout wire scl;
    inout wire sda;
    
    // Tri State SCL and SDA
    wire sda_in;
	reg sda_o = 1;
	
	wire sda_oe = ~sda_o;
    
    assign sda = sda_oe ? 1'b0 : 1'bz; // We only tie at ground
    assign sda_in = sda;
    
    wire scl_in;
    reg scl_q = 0;
    reg scl_oe = 0;
    reg scl_en = 0; // needed to emit scl only when wanted
    
    assign scl = (scl_oe && scl_en) ? 1'b0 : 1'bz;
    assign scl_in = scl;
    
    wire scl_stretch = ((scl_oe == 0) && (scl_in == 0));
    
    // Generate EDGE
    reg [15:0]i2c_cnt = CLK_DELAY;
	reg [15:0]i2c_clk_90 = 0;
	
    reg scl_l = 0;
    reg scl_h = 0;
	reg virtual_scl = 1;
    
	reg scl_r_edge_out = 0;
	reg scl_f_edge_out = 0;
	
    always @(posedge clk)
    begin
        scl_l <= 0;
        scl_h <= 0;
        
        //if(!scl_stretch) begin
            i2c_cnt <= i2c_cnt + 16'd1;
			
			i2c_clk_90 <= i2c_clk_90 + 16'd1;
        
            if(i2c_cnt == CNT_CLK - 1) begin
                i2c_cnt <= 0;
                if(virtual_scl == 1'b0) // Clock = 0
                    scl_l <= 1'b1;
                else
                    scl_h <= 1'b1;
            end
			
			if(scl_l) // reversed because here it's a register
                virtual_scl <= 1'b1;
            else if(scl_h)
                virtual_scl <= 1'b0;
            
        //end
    end
	
	reg [15:0]r_delay_cnt = 0;
	reg [15:0]f_delay_cnt = 0;
	
	reg r_delay_run = 0;
	reg f_delay_run = 0;
	
	always @(posedge clk)
	begin
		scl_r_edge_out <= 1'b0;
		scl_f_edge_out <= 1'b0;
		
		if(scl_l) begin
			r_delay_run <= 1;
		end else if(r_delay_run) begin
			r_delay_cnt <= r_delay_cnt + 1;
			if(r_delay_cnt == CLK_DELAY - 1) begin
				r_delay_run <= 0;
				scl_r_edge_out <= 1;
				r_delay_cnt <= 0;
			end
		end
		
		if(scl_h) begin
			f_delay_run <= 1;
		end else if(f_delay_run) begin
			f_delay_cnt <= f_delay_cnt + 1;
			if(f_delay_cnt == CLK_DELAY - 1) begin
				f_delay_run <= 0;
				scl_f_edge_out <= 1;
				f_delay_cnt <= 0;
			end
		end
		
		if(scl_r_edge_out)
			scl_oe <= 1'b0;
		else if(scl_f_edge_out)
			scl_oe <= 1'b1;
	end
	
	// Generate read and write
	wire [7:0]devaddr_w = {devaddr, 1'b0};
	wire [7:0]devaddr_r = {devaddr, 1'b1};
    
    // I2C FSM
    
    reg [3:0]i2c_fsm_state = 0;
	reg [7:0]i = 0;
	reg [3:0]fsm_cnt = 0;
	reg need_ack = 0;
    
    localparam I2C_IDLE         = 4'd0;
    localparam I2C_START        = 4'd1;
	localparam I2C_DEVADDR_W	= 4'd2;
	localparam I2C_WORD_ADDR	= 4'd3;
	localparam I2C_REPEAT_START	= 4'd4;
	localparam I2C_DEVADDR_R	= 4'd5;
	localparam I2C_READ_DATA	= 4'd6;
	localparam I2C_WRITE_DATA	= 4'd7;
	localparam I2C_STOP			= 4'd8;
	
    always @(posedge clk)
    begin
		done <= 1'b0;
        case(i2c_fsm_state)
            
            I2C_IDLE: begin
				done <= 1'b1;
                if(start) begin
					fsm_cnt <= 0;
                    i2c_fsm_state <= I2C_START;
                end
            end
            
            I2C_START: begin
				// We push the DATA @0 when SCK rise
				if(scl_h) begin
					sda_o <= 0; // We drive data low
					i2c_fsm_state <= I2C_DEVADDR_W;
					// Enable clock output
					scl_en <= 1;
				end
            end
			
			I2C_DEVADDR_W: begin
				// We push address bit on falling edge
				if(scl_l) begin
					fsm_cnt <= fsm_cnt + 1;
					sda_o <= devaddr_w[7-fsm_cnt];
					if(fsm_cnt == 8) // we will read ACK at rising edge
						sda_o <= 1'b1; // release SDA
				end else if(scl_r_edge_out && fsm_cnt == 9) begin
					nack <= sda_in;
					fsm_cnt <= 0;
					i2c_fsm_state <= (sda_in) ? I2C_IDLE : I2C_WORD_ADDR;
				end
			end
			
			I2C_WORD_ADDR: begin
				// We push word address on falling edge
				if(scl_l) begin
					fsm_cnt <= fsm_cnt + 1;
					sda_o <= word_addr[7-fsm_cnt];
					if(fsm_cnt == 8)
						sda_o <= 1'b1;
				end else if(scl_r_edge_out && fsm_cnt == 9) begin
					nack <= sda_in;
					fsm_cnt <= 0;
					if(!sda_in)
						i2c_fsm_state <= (read) ? I2C_REPEAT_START : I2C_WRITE_DATA;
					else
						i2c_fsm_state <= I2C_IDLE;
				end
			end
			
			I2C_REPEAT_START: begin
				// We tie low SDA during SCL H
				if(scl_h) begin
					fsm_cnt <= 1;
					if(fsm_cnt == 1) begin
						sda_o <= 0;
						fsm_cnt <= 0;
						i2c_fsm_state <= I2C_DEVADDR_R;
					end
				end
			end
            
			I2C_DEVADDR_R: begin
				// We push address bit on falling edge
				if(scl_l) begin
					fsm_cnt <= fsm_cnt + 1;
					sda_o <= devaddr_r[7-fsm_cnt];
					if(fsm_cnt == 8) // we will read ACK at rising edge
						sda_o <= 1'b1; // release SDA
				end else if(scl_r_edge_out && fsm_cnt == 9) begin
					nack <= sda_in;
					fsm_cnt <= 0;
					i2c_fsm_state <= (sda_in) ? I2C_IDLE : I2C_READ_DATA;
				end
			end
			
			I2C_READ_DATA: begin
				// Read on the rising edge
				data_valid <= 0;
				if (scl_r_edge_out && !need_ack) begin
					fsm_cnt <= fsm_cnt + 1;
					data_rd <= {data_rd[6:0], sda_in};
					if(fsm_cnt == 7) begin
						need_ack <= 1;
						fsm_cnt <= 0;
						i <= i + 1;
						data_valid <= 1;
					end
				end else if(scl_l && need_ack) begin
					sda_o <= 0; // Yes WE ACK !
					if(i == len) begin // Len to read
						sda_o <= 1; // No WE NACK !
						i <= 0;
						need_ack <= 0;
						i2c_fsm_state <= I2C_STOP;
					end
				end else if(scl_l && !need_ack) begin
					sda_o <= 1; // We remove previous ACK state
				end if (scl_r_edge_out && need_ack) begin
					need_ack <= 0;
				end
			end
			
			I2C_WRITE_DATA: begin
				// To be implemented
			end
			
			I2C_STOP: begin
				// We drive data at 0 after NACK
				if(scl_l) begin
					sda_o <= 0;
					fsm_cnt <= 1;
				end else if(scl_r_edge_out && fsm_cnt == 1) begin
					scl_en <= 0;
				end else if(scl_h && fsm_cnt == 1) begin
                    sda_o <= 1;
                    fsm_cnt <= 0;
                    i2c_fsm_state <= I2C_IDLE;
                end
			end
			
        endcase
    end
    
endmodule
