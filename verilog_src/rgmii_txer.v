`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:18:07 07/04/2016 
// Design Name: 
// Module Name:    rgmii_txer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rgmii_txer(// Physical Interface
						RXC, RXD, RXCTL,
						TXC, TXD, TXCTL,
						
						// FPGA side
						// GMII RX - clock recovered from RXC
						GMII_RXD, GMII_RXDV, GMII_RXER,
						
						// GMII TX - clock provided by the logic
						GMII_TXC,
						GMII_TXD, GMII_TXEN, GMII_TXER
    );
	// RGMII PHY Side
    input wire RXC;
    input wire [3:0]RXD;
    input wire RXCTL;
    
	output wire TXC;
    output wire [3:0]TXD;
    output wire TXCTL;
	 
	// GMII FPGA Side
	output wire [7:0]GMII_RXD;
	output wire GMII_RXDV;
	output wire GMII_RXER;
	
	input wire GMII_TXC;
	input wire [7:0]GMII_TXD;
	input wire GMII_TXEN;
	input wire GMII_TXER;
	 
	// RGMII RX Side
	// RXD => IODelay => IODDR
	// 39 taps => 2039ps delay on S6 -2 (see ds162 table 39)
	// 4*8 + 1*7 taps => 4*424ps + 343ps
	// Be carefull some phy (Marvell 88e1512) add delay by default !

	wire [3:0]rxd_delayed;

	IO_BusDelay #(
		.SIZE(4),
		.NB_TAPS(0),
		.DIRECTION("INPUT"))
	rx_delay_block (
		.iob_in(RXD),
		.oser_in(),
		.fabric_out(),
		.iser_out(rxd_delayed),
		.iob_out()
	);

	IO_BusDDR #(
		.SIZE(4),
		.DIRECTION("INPUT"))
	rx_ddr_block (
		//.ddr_in(rxd_delayed), bug ?
        .ddr_in(RXD),
		.clk_in_p(RXC),
		.clk_in_n(~RXC),
		.sdr_0_out(GMII_RXD[7:4]),
		.sdr_1_out(GMII_RXD[3:0])
	);

	wire rxctl_delayed;
	wire rgmii_rxerr;

	IO_BusDelay #(
		.SIZE(1),
		.NB_TAPS(0),
		.DIRECTION("INPUT"))
	rxctl_delay_block (
		.iob_in(RXCTL),
		.oser_in(),
		.fabric_out(),
		.iser_out(rxctl_delayed),
		.iob_out()
	);

	// See RGMII spec section 3.4

	assign GMII_RXER = GMII_RXDV ^ rgmii_rxerr;

	IO_BusDDR #(
		.SIZE(1),
		.DIRECTION("INPUT"))
	rxctl_ddr_block (
		//.ddr_in(rxctl_delayed), bug?
        .ddr_in(RXCTL),
		.clk_in_p(RXC),
		.clk_in_n(~RXC),
		.sdr_0_out(rgmii_rxerr),
		.sdr_1_out(GMII_RXDV)
	);
	
	// RGMII TX Side
	
	wire txc_buf;
	
	IO_BusDDR #(
		.SIZE(1),
		.DIRECTION("OUTPUT"))
	txc_output (
		.clk_in_p(GMII_TXC),
		.clk_in_n(~GMII_TXC),
		.sdr_0_in(1'b1),
		.sdr_1_in(1'b0),
		.ddr_out(txc_buf)
	);
	
	IO_BusDelay #(
		.SIZE(1),
		.NB_TAPS(0),
		.DIRECTION("OUTPUT"))
	txctl_delay_block (
		.iob_in(),
		.oser_in(txc_buf),
		.fabric_out(),
		.iser_out(),
		.iob_out(TXC)
	);
	
	IO_BusDDR #(
		.SIZE(4),
		.DIRECTION("OUTPUT"))
	txd_output (
		.clk_in_p(GMII_TXC),
		.clk_in_n(~GMII_TXC),
		.sdr_0_in(GMII_TXD[3:0]),
		.sdr_1_in(GMII_TXD[7:4]),
		.ddr_out(TXD)
	);
	
	IO_BusDDR #(
		.SIZE(1),
		.DIRECTION("OUTPUT"))
	txctl_output (
		.clk_in_p(GMII_TXC),
		.clk_in_n(~GMII_TXC),
		.sdr_0_in(GMII_TXEN),
		.sdr_1_in(GMII_TXEN ^ GMII_TXER),
		.ddr_out(TXCTL)
	);

endmodule
