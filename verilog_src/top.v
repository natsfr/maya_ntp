`timescale 1ns / 1ps
`default_nettype none

module top(
    input wire clk50,
	 
    inout wire mdio,
    output wire mdc,
    
	 output wire uart_tx,
	 
	 input wire RXC,
	 input wire [3:0]RXD,
	 input wire RXCTL,
	 
	 inout wire SCL,
	 inout wire SDA,
	 input wire ABS,
	 
	 output reg led1,
	 output wire led0,
	 output wire dsig,
	 output wire dsig2,
	 output wire dsig3
    );
	 
	 // BUF clk50
	 wire clk50_buf;
	 IBUFG in_clk50(.I(clk50), .O(clk50_buf));

	// Generate the 125MHz
	
	wire raw_pll_out;
	wire clk125;
	
	PLL_BASE #(
		.BANDWIDTH("OPTIMIZED"),
		.CLK_FEEDBACK("CLKOUT0"),
		.CLKFBOUT_MULT(5),
		.CLKOUT0_DIVIDE(2),
		.CLKIN_PERIOD(20.0)
	) pll_125gen (
		.CLKIN(clk50_buf),
		.RST(1'b0),
		.CLKFBIN(clk125),
		
		.CLKFBOUT(),
		
		.CLKOUT0(raw_pll_out),
		.CLKOUT1(),
		.CLKOUT2(),
		.CLKOUT3(),
		.CLKOUT4(),
		.CLKOUT5()
	);
	
	BUFG clk125_bufg(.I(raw_pll_out), .O(clk125));
	
	//General parameters
	localparam SECOND					= 50000000;
	reg [25:0]second_cnt 			= 0;
	
	// Instantiate IP
	
	// ================ I2C  =================== //
	// SFF-8472 Rev 11.0
	
	parameter SFP_A0				= 8'hA0;
	parameter SFP_A2				= 8'hA2;
	
	// A0h Field
	
	
	// A2h Field
	parameter SFP_POW_MSB		= 8'd104;
	parameter SFP_POW_LSB		= 8'd105;
	
	reg [15:0]opt_power = 0;
	reg [15:0]q_opt_power = 0;
	
	reg i2c_start = 0;
	reg i2c_read = 0;
	
	wire [7:0]i2c_data_r;
	wire i2c_done;
	wire i2c_nack;
	wire i2c_valid;
	
	simple_i2c_master sfp_i2c(.clk(clk50_buf), .start(i2c_start), .read(i2c_read), .devaddr(SFP_A2[7:1]), .data_wr(8'd0),
										.word_addr(SFP_POW_MSB), .len(8'd2), .data_valid(i2c_valid), .data_rd(i2c_data_r), .done(i2c_done),
										.nack(i2c_nack), .scl(SCL), .sda(SDA));
	
	reg i2c_cnt = 0;
	reg [3:0]i2c_fsm = 0;
	
	localparam I2C_IDLE 				= 0;
	localparam I2C_MSB				= 1;
	localparam I2C_LSB				= 2;
	localparam I2C_LATCH				= 3;
	
	always @(posedge clk50_buf)
	begin
		i2c_start <= 0;
		i2c_read <= 1;
		
		if(i2c_nack) i2c_fsm <= I2C_IDLE;

		case(i2c_fsm)
			I2C_IDLE: begin
				if(second_cnt == SECOND) begin
					i2c_start <= 1;
					i2c_fsm <= I2C_MSB;
				end
			end
			
			I2C_MSB: begin
				if(i2c_valid) begin
					q_opt_power[15:8] <= i2c_data_r;
					i2c_fsm <= I2C_LSB;
				end
			end
			
			I2C_LSB: begin
				if(i2c_valid) begin
					q_opt_power[7:0] <= i2c_data_r;
					i2c_fsm <= I2C_LATCH;
				end
			end
			
			I2C_LATCH: begin
				if(i2c_done) begin
					opt_power <= q_opt_power;
					i2c_fsm <= I2C_IDLE;
				end
			end
		endcase
		
	end
	
	// ================ UART =================== //
	reg [7:0]dsend = 8'd52;
	reg u_start = 0;
	
	wire u_done;

	uart_tx #(.BAUD(9600), .CLK_SPEED(50000000))
				uart_debug(.clk(clk50_buf), .start(u_start),
				.dsend(dsend), .bit_out(uart_tx), .done(u_done));

	// ================ MDIO =================== //
	reg phy_rd = 0;
	reg phy_wr = 0;
	
	wire mdio_busy;
	
	reg [4:0]reg_addr = 0;
	reg [15:0]data_wr = 0;
	wire [15:0]data_rd;

	MDIO_transceiver mdio_88e1512(.clk(clk50_buf), .mdio(mdio), .mdc(mdc),
							.phy_rd(phy_rd), .phy_wr(phy_wr), .mdio_busy(mdio_busy),
							.reg_addr(reg_addr), .data_wr(data_wr), .data_rd(data_rd));
							
	// ================ RGMI =================== //
	
	wire [10:0]fr_byte_index;
	wire fr_wr_en;
	wire [7:0]frame_byte;
	wire valid_eth_frame;
	
	reg frame_used = 0;
	
	udld_frame_handler rgmii_phy(.clk(clk125),
						.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL),
						.bram_wr_en(fr_wr_en), .bram_index(fr_byte_index), .bram_data(frame_byte),
						.valid_frame_rx(valid_eth_frame), .frame_used(frame_used),
						.led0(led0), .led1());
	
	// Dual gate frame valid and frame length
	// Frame length should be ideally FIFOed
	reg q_fr_valid = 0;
	reg u_fr_valid = 0;
	
	reg [10:0]q_fr_length = 0;
	reg [10:0]u_fr_length = 0;
	
	always @(posedge clk50_buf)
	begin
		q_fr_valid <= valid_eth_frame;
		u_fr_valid <= q_fr_valid;
		
		led1 <= u_fr_valid;
		
		q_fr_length <= fr_byte_index;
		u_fr_length <= q_fr_length;
	end
						
	// ================ BRAM =================== //
	
	reg [10:0]fr_send_index = 0;
	wire [7:0]fr_send_byte;
	
	BRAM_WRAP #(.WIDTH(8), .DEPTH(1500)) udld_frame(.clk(RXC), .en(1'b1), .addr(fr_byte_index),
													.we(fr_wr_en), .din(frame_byte), .dout(),
													.clkb(clk50_buf), .enb(1'b1), .addrb(fr_send_index), 
													.doutb(fr_send_byte));

	// FSM module control
	wire mdio_ready = !phy_wr && !phy_rd && !mdio_busy;
	wire uart_ready = u_done && !u_start;
	// FSM to read mdio data and send to uart
	
	// First configure the PHY mode register 20_18 | 88E1512 start in auto detect mode
	
	// Read sync flag in loop for a first test
	// Need to select page of regiter by writing page number in register 22
	
	// Fiber sync flage: page 1 | reg 17
	
	reg [2:0] 	main_fsm_state = 0;
	reg [2:0] 	fsm_cnt = 0;
	reg [10:0]	frame_size = 0;
	
	reg sync_bit = 0;
	
	localparam IDLE					= 0;
	localparam SEND_FRAME			= 1;
	localparam PARKING 				= 2;
	localparam WAIT_FRAME			= 3;
	localparam SEND_HEADER			= 4;
	localparam SEND_UART_PREAMBLE	= 5;
	
	assign dsig = mdio_busy;
	assign dsig2 = u_done;
	assign dsig3 = u_start;
	
	// UART FSM	
	always @(posedge clk50_buf)
	begin
		case(main_fsm_state)
			IDLE: begin
				u_start <= 0;
				fsm_cnt <= 0;
				if(uart_ready) begin
					main_fsm_state <= WAIT_FRAME;
				end
			end
			
			WAIT_FRAME: begin
				u_start <= 0;
				frame_used <= 0;
				second_cnt <= second_cnt + 26'd1;
				if(second_cnt == SECOND) begin
					second_cnt <= 0;
					fsm_cnt <= 0;
					main_fsm_state <= SEND_UART_PREAMBLE;
				end
			end
			
			SEND_UART_PREAMBLE: begin
				u_start <= 0;
				if(uart_ready) begin
					fsm_cnt <= fsm_cnt + 3'd1;
					u_start <= 1;
					dsend <= 8'hAA;
					if(fsm_cnt == 3'd3) begin
						fsm_cnt <= 0;
						main_fsm_state <= SEND_HEADER;
					end
				end
			end
			
			SEND_HEADER: begin
				u_start <= 0;
				if(uart_ready) begin
					fsm_cnt <= fsm_cnt + 3'd1;
					u_start <= 1;
					case(fsm_cnt)
						0: dsend <= {1'b1, {4{1'b0}}, u_fr_valid, sync_bit, !ABS};
						1: dsend <= opt_power[15:8];
						2: dsend <= opt_power[7:0];
						3: dsend <= {{5{1'b0}}, u_fr_length[10:8]};
						default: begin
							fsm_cnt <= 0;
							dsend <= u_fr_length[7:0];
							main_fsm_state <= (u_fr_valid) ? SEND_FRAME : WAIT_FRAME;
						end
					endcase
				end
			end
			
			SEND_FRAME: begin
				u_start <= 0;
				frame_used <= 0;
				if(uart_ready && u_fr_valid && !frame_used) begin
					fr_send_index <= fr_send_index + 1;
					dsend <= fr_send_byte;
					u_start <= 1;
					if(fr_send_index == u_fr_length) begin // Use last byte index as the frame length
						frame_used <= 1;
						fr_send_index <= 0;
						main_fsm_state <= WAIT_FRAME;
					end
				end
			end
			
			PARKING: begin
				main_fsm_state <= PARKING;
				u_start <= 1'b0;
			end
		endcase
	end
	
	// MDIO FSM
	
	reg [1:0]sync_fsm_state = 0;
	
	localparam READ_SYNC				= 1;
	localparam SET_PAGE1				= 2;
	localparam CHECK_SYNC			= 3;

	always @(posedge clk50_buf)
	begin
		case(sync_fsm_state)
			IDLE: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE1;
				end
			end
			
			SET_PAGE1: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_wr <= 1'b1;
					reg_addr <= 5'd22;
					data_wr <= 16'd1;
					sync_fsm_state <= READ_SYNC;
				end
			end
			
			READ_SYNC: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					phy_rd <= 1'b1;
					reg_addr <= 5'd17;
					sync_fsm_state <= CHECK_SYNC;
				end
			end
			
			CHECK_SYNC: begin
				phy_wr <= 0;
				phy_rd <= 0;
				if(mdio_ready) begin
					sync_fsm_state <= SET_PAGE1;
					if(data_rd[5] == 1) begin // We got a 8b/10b sync
						// light LED to indicate sync
						// activate the SFP tx if needed
						sync_bit <= 1;
					end else begin
						// no sync, turn off led and sfp tx
						sync_bit <= 0;
					end
				end
			end
		endcase
	end

endmodule
