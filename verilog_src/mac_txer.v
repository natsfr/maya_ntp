`timescale 1ns / 1ps
`default_nettype none

module mac_txer(
    // TX interface
    input wire tx_fifo_wr,
    input wire [7:0]tx_din,
    
    output wire tx_fifo_full,
    
    input wire [31:0]gps_time_utc,
    input wire [31:0]gps_time_second,
    
    // RX Interface
    input wire rx_fifo_rd,
    
    output reg [7:0]rx_fifo_data = 0,
    output wire rx_fifo_empty,
    output reg rx_frame_done = 0,
    
    // RGMII Interface
    input wire RXC,
    input wire [3:0]RXD,
    input wire RXCTL,
    
    output wire TXC,
    output wire [3:0]TXD,
    output wire TXCTL,
    
    // RX Recovered Clock
    output reg ledr = 0,
    output wire clk_125MHz,
    
    input wire reset);
    
    // S6: 18kb BRAM Size
    parameter FIFO_SIZE = 16'd18000;
    parameter LEN_FIFO_SIZE = 8'd35; // maximum 35 frame of 64 byte len can be stored in 18kb
    
    // Ethernet define
    parameter ETH_PREAMBLE                          = 8'b01010101; // 0x55
    parameter ETH_SFD                               = 8'b11010101; // 0xD5
    
    // We use the RX recovered clock for everything
    // that's not "good" but allow to have no jitter
    // for ntp.
    assign clk_125MHz = RXC;
    
    // RGMII interface
    
    wire gmii_rxv, gmii_rxerr;
    wire [7:0]gmii_rxd;
    reg gmii_txen = 0;
    reg gmii_txer = 0;
    reg [7:0]gmii_txd = 0;
    
    rgmii_txer rgmii_to_gmii(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL),
                            .TXC(TXC), .TXD(TXD), .TXCTL(TXCTL),
                            
                            .GMII_RXD(gmii_rxd), .GMII_RXDV(gmii_rxv), .GMII_RXER(gmii_rxerr),
                            
                            .GMII_TXC(RXC), .GMII_TXD(gmii_txd), .GMII_TXEN(gmii_txen), .GMII_TXER(gmii_txer));
    
    // ----------------------------------------------------------------------------
    // RX Part
    // ----------------------------------------------------------------------------
        reg [7:0]rx_fifo[0:FIFO_SIZE-1];
        reg [15:0]rx_fifo_len[0:LEN_FIFO_SIZE-1];
        
        reg [5:0]rx_len_wr_p = 0;
        reg [5:0]rx_len_rd_p = 0;
        reg [15:0]rx_len_cnt = 0;
        
        reg [15:0]rx_fifo_wr_p = 0;         // Current FIFO write pointer
        reg [15:0]rx_tmp_wr_p = 0;        // Used in case we need to cancel the frame processing
        
        reg [15:0]rx_fifo_rd_p = 0;
        
        // RX Fifo is full, we drop the frame.
        wire rx_fifo_full = (rx_tmp_wr_p == rx_fifo_rd_p - 16'd1) | ((rx_tmp_wr_p == FIFO_SIZE - 1) & (rx_fifo_rd_p == 0));
        assign rx_fifo_empty = (rx_fifo_rd_p == rx_fifo_wr_p);
        
        reg [1:0]rx_state = 0;
        localparam RX_IDLE          = 0;
        localparam RX_DATA          = 1;
        localparam RX_DROP          = 2;
        
        always @(posedge RXC)
        begin
            case(rx_state)
                RX_IDLE: begin
                    rx_len_cnt <= 0;
                    
                    if(gmii_rxv) begin
                        ledr <= ~ledr;
                        if(gmii_rxerr)
                            rx_state <= RX_DROP;
                        else if(gmii_rxd == ETH_SFD) begin
                            rx_state <= RX_DATA;
                        end
                    end
                end
                
                RX_DATA: begin
                    if(gmii_rxv) begin // We should add filter here to drop unwanted frame
                        rx_len_cnt <= rx_len_cnt + 16'd1;
                        rx_tmp_wr_p <= (rx_tmp_wr_p == FIFO_SIZE - 1) ? 16'd0 : rx_tmp_wr_p + 16'd1;
                        rx_fifo[rx_tmp_wr_p] <= gmii_rxd;
                        if(gmii_rxerr | rx_fifo_full) rx_state <= RX_DROP;
                    end else begin // Finished valid transmission
                        rx_fifo_len[rx_len_wr_p] <= rx_len_cnt;
                        rx_len_wr_p <= (rx_len_wr_p == LEN_FIFO_SIZE - 1) ? 6'd0 : rx_len_wr_p + 6'd1; // We save the next len write position
                        rx_fifo_wr_p <= rx_tmp_wr_p; // We save the next write position
                        rx_state <= RX_IDLE;
                    end
                end
                
                RX_DROP: begin // We wait gmii_rxv to go low before going IDLE
                    if(!gmii_rxv) rx_state <= RX_IDLE;
                end
            endcase
        end
    
        // RX Read
        reg first_byte = 1; // indicate first byte read
        reg [15:0]read_len = 0;
        reg [15:0]read_cnt = 0;
        
        always @(posedge RXC)
        begin
            first_byte <= 1;
            rx_frame_done <= 0;
            read_cnt <= 0;
            
            rx_fifo_data <= rx_fifo[rx_fifo_rd_p];
            
            if(rx_fifo_rd && !rx_fifo_empty) begin
                rx_fifo_rd_p <= rx_fifo_rd_p + 16'd1;
                read_cnt <= read_cnt + 16'd1;
                
                first_byte <= 0;
                if(first_byte) begin
                    read_len <= rx_fifo_len[rx_len_rd_p]; // We get the corresponding length
                    rx_len_rd_p <= (rx_len_rd_p == LEN_FIFO_SIZE-1) ? 6'd0 : rx_len_rd_p + 6'd1;
                end else begin
                    rx_frame_done <= (read_len == read_cnt);
                end
            end
        end
    
    // ----------------------------------------------------------------------------
    // TX Part
    // ----------------------------------------------------------------------------
        reg [7:0]tx_fifo[0:FIFO_SIZE-1];
        reg [15:0]tx_fifo_len[0:LEN_FIFO_SIZE-1];
        
        reg [5:0]tx_len_wr_p = 0;
        reg [5:0]tx_len_rd_p = 0;
        reg [15:0]tx_len_cnt = 0;
        reg [15:0]tx_frame_len = 0;

        reg [15:0]tx_fifo_wr_p = 0;         // Current FIFO write pointer
        reg [15:0]tx_tmp_wr_p = 0;        // Used in case we need to cancel the frame processing
        
        reg [15:0]tx_fifo_rd_p = 0;
          
        reg [7:0] tx_data = 0;
        reg crc_rst = 1;
        reg [31:0]crc_r = 0;
        wire [31:0]crc_w;
        
        eth_crc32x8 tx_crc(.clk(RXC), .data(tx_data), .rst(crc_rst), .crc(crc_w));
        
        reg [2:0]tx_state = 0;
        reg [15:0]tx_fsm_cnt = 0;
        
        localparam TX_IDLE          = 0;
        localparam TX_PREAMBLE      = 1;
        localparam TX_SFD           = 2;
        localparam TX_DATA          = 3;
        localparam TX_CRC           = 4;
        
        always @(posedge RXC)
        begin
            gmii_txen <= 0;
            gmii_txer <= 0;
            crc_rst <= 1;
            
            tx_data <= tx_fifo[tx_fifo_rd_p];
            
            case(tx_state)
                TX_IDLE: begin
                    tx_fsm_cnt <= 0;
                    if(!tx_fifo_empty) begin
                        tx_state <= TX_PREAMBLE;
                        tx_frame_len <= tx_fifo_len[tx_len_rd_p];
                        tx_len_rd_p <= (tx_len_rd_p == LEN_FIFO_SIZE-1) ? 6'd0 : tx_len_rd_p + 6'd1;
                    end
                end
                
                TX_PREAMBLE: begin
                    tx_fsm_cnt <= tx_fsm_cnt + 16'd1;
                    gmii_txen <= 1;
                    gmii_txd <= ETH_PREAMBLE;
                    if(tx_fsm_cnt == 7) begin
                        tx_fsm_cnt <= 0;
                        tx_state <= TX_SFD;
                    end
                end
                
                TX_SFD: begin
                    gmii_txen <= 1;
                    gmii_txd <= ETH_SFD;
                    // Start the CRC calculation one cycle earlier
                    // we push data in CRC and increment counter
                    crc_rst <= 0;
                    tx_fsm_cnt <= 1;
                    tx_fifo_rd_p <= (tx_fifo_rd_p == FIFO_SIZE-1) ? 16'd0 : tx_fifo_rd_p + 16'd1;
                    tx_state <= TX_DATA;
                end
                
                TX_DATA: begin
                    gmii_txen <= 1;
                    gmii_txd <= tx_data;
                    
                    crc_rst <= 0;
                    tx_fsm_cnt <= tx_fsm_cnt + 16'd1;
                    
                    tx_fifo_rd_p <= (tx_fifo_rd_p == FIFO_SIZE-1) ? 16'd0 : tx_fifo_rd_p + 16'd1; // The read pointer will stop on the next frame start
                    
                    if(tx_fsm_cnt == tx_frame_len) begin
                        tx_state <= TX_CRC;
                        tx_fsm_cnt <= 0;
                    end
                end
                
                TX_CRC: begin
                    gmii_txen <= 1;
                    crc_r <= {crc_w[23:0], crc_w[31:24]};
                    gmii_txd <= crc_w[31:24];
                    tx_fsm_cnt <= tx_fsm_cnt + 16'd1;
                    if(tx_fsm_cnt != 0) begin
                        crc_r <= {crc_r[23:0], crc_r[31:24]};
                        gmii_txd <= crc_r[31:24];
                        if(tx_fsm_cnt == 3) tx_state <= TX_IDLE;
                    end
                end
            endcase

        end
        
        // TX Write
        
        assign tx_fifo_full = (tx_tmp_wr_p == tx_fifo_rd_p - 16'd1) | ((tx_tmp_wr_p == FIFO_SIZE - 1) & (tx_fifo_rd_p == 0));
        
        wire tx_fifo_empty = (tx_fifo_rd_p == tx_fifo_wr_p);
        
        reg tx_write_busy = 0;
        
        always @(posedge RXC)
        begin
            tx_len_cnt <= 0;
            tx_write_busy <= 0;
            
            if(tx_fifo_wr) begin
                tx_write_busy <= 1;
                if(!tx_write_busy) tx_tmp_wr_p <= tx_fifo_wr_p; // Set the tmp pointer
                if(!tx_fifo_full) begin // When fifo is full upper layer should stop pushing data
                    tx_len_cnt <= tx_len_cnt + 16'd1;
                    tx_tmp_wr_p <= (tx_tmp_wr_p == FIFO_SIZE-1) ? 16'd0 : tx_tmp_wr_p + 16'd1;
                    tx_fifo[tx_tmp_wr_p] <= tx_din;
                end
            end else if(tx_write_busy) begin // We just finished to get the frame
                tx_fifo_wr_p <= tx_tmp_wr_p; // We are directly at start of next frame
                tx_fifo_len[tx_len_wr_p] <= tx_len_cnt + 16'd1;
                tx_len_wr_p <= (tx_len_wr_p == LEN_FIFO_SIZE-1) ? 6'd0 : tx_len_wr_p + 6'd1;
            end
        end
        
endmodule
