`timescale 1ns / 1ps
`default_nettype none

module mac_txer(
    // TX interface
    input wire tx_fifo_wr,
    input wire tx_fifo_dv,
    input wire tx_fifo_dc,
    input wire [7:0]tx_din,
    
    output wire tx_fifo_full,
    
    // RX Interface
    input wire rx_fifo_rd,
    
    output wire [7:0]rx_data_out,
    output wire rx_fifo_empty,
    output wire rx_frame_done,
    
    // RGMII Interface
    input wire RXC,
    input wire [3:0]RXD,
    input wire RXCTL,
    
    output wire TXC,
    output wire [3:0]TXD,
    output wire TXCTL,
    
    // RX Recovered Clock
    output reg ledr = 0,
    output wire clk_125MHz,
    
    // Config port
    input wire prom_mode,
    input wire [47:0]MAC,
    
    input wire [31:0]gps_time_utc,
    input wire [31:0]gps_time_second,
    
    input wire reset);
    
    wire [47:0]MAC_BCAST = 48'hFFFFFFFFFFFF;
    
    // S6: 18kb BRAM Size
    parameter FIFO_SIZE = 16'd16384;
    parameter LEN_FIFO_SIZE = 8'd32; // maximum 32 frame of 64 byte len can be stored in 18kb
    
    // Ethernet define
    parameter ETH_PREAMBLE                          = 8'b01010101; // 0x55
    parameter ETH_SFD                               = 8'b11010101; // 0xD5
    
    // We use the RX recovered clock for everything
    // that's not "good" but allow to have no jitter
    // for ntp.
    assign clk_125MHz = RXC;
    
    // RGMII interface
    
    wire gmii_rxv, gmii_rxerr;
    wire [7:0]gmii_rxd;
    reg gmii_txen = 0;
    reg gmii_txer = 0;
    reg [7:0]gmii_txd = 0;
    
    rgmii_txer rgmii_to_gmii(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL),
                            .TXC(TXC), .TXD(TXD), .TXCTL(TXCTL),
                            
                            .GMII_RXD(gmii_rxd), .GMII_RXDV(gmii_rxv), .GMII_RXER(gmii_rxerr),
                            
                            .GMII_TXC(RXC), .GMII_TXD(gmii_txd), .GMII_TXEN(gmii_txen), .GMII_TXER(gmii_txer));
    
    wire [35:0]CONTROL0;
    
    // chipscope_icon icon_inst (
        // .CONTROL0(CONTROL0) // INOUT BUS [35:0]
    // );
    
    // chipscope_ila ila_inst (
        // .CONTROL(CONTROL0), // INOUT BUS [35:0]
        // .CLK(RXC), // IN
        // .DATA(gmii_txd), // IN BUS [7:0]
        // .TRIG0(gmii_txen), // IN BUS [0:0]
        // .TRIG1(gmii_txd) // IN BUS [7:0]
    // );
    
    // ----------------------------------------------------------------------------
    // RX Part
    // ----------------------------------------------------------------------------
        
        reg rx_fifo_wr = 0;
        reg rx_fifo_dv = 0;
        reg rx_fifo_dc = 0;
        reg [7:0]rx_data_in = 0;
        
        wire rx_fifo_full;
        
        packet_fifo #(.TRIM_END(4)) rx_fifo(.clk(RXC), .data_wr(rx_fifo_wr), .data_in(rx_data_in),
                                    .data_valid(rx_fifo_dv), .data_cancel(rx_fifo_dc), 
                                    .data_rd(rx_fifo_rd), .packet_done(rx_frame_done), .data_out(rx_data_out),
                                    .fifo_empty(rx_fifo_empty), .fifo_full(rx_fifo_full));
        
        localparam RX_IDLE          = 0;
        localparam RX_DATA          = 1;
        localparam RX_DROP          = 2;
        
        reg [1:0]rx_state = RX_IDLE;
        reg [7:0]fsm_cnt = 0;
        
        reg [47:0]mac_check = 0;
        
        always @(posedge RXC)
        begin
            rx_fifo_wr <= 0;
            rx_fifo_dv <= 0;
            rx_fifo_dc <= 0;
            
            fsm_cnt <= 0;
            
            mac_check <= 0;
            
            case(rx_state)
                RX_IDLE: begin
                    if(gmii_rxv) begin
                        if(gmii_rxerr)
                            rx_state <= RX_DROP;
                        else if(gmii_rxd == ETH_SFD) begin
                            rx_state <= RX_DATA;
                        end
                    end
                end
                
                RX_DATA: begin
                    if(gmii_rxv) begin // We should add filter here to drop unwanted frame
                        if(gmii_rxerr | rx_fifo_full) rx_state <= RX_DROP;
                        rx_data_in <= gmii_rxd;
                        rx_fifo_wr <= 1;
                        
                        // MAC Filtering
                        if(!prom_mode) begin
                            fsm_cnt <= fsm_cnt + 8'd1;
                            mac_check <= {mac_check[39:0], gmii_rxd};
                            if(fsm_cnt == 6) begin
                                if(mac_check != MAC & mac_check != MAC_BCAST)
                                    rx_state <= RX_DROP;
                            end else begin
                                fsm_cnt <= fsm_cnt + 8'd1;
                            end
                        end
                        
                    end else begin // Finished valid transmission
                        rx_state <= RX_IDLE;
                        rx_fifo_dv <= 1;
                    end
                end
                
                RX_DROP: begin // We wait gmii_rxv to go low before going IDLE
                    if(!gmii_rxv) rx_state <= RX_IDLE;
                    rx_fifo_dc <= 1;
                end
            endcase
        end
    
    // ----------------------------------------------------------------------------
    // TX Part
    // ----------------------------------------------------------------------------
        
        reg tx_fifo_rd = 0;
        
        wire tx_frame_done;
        wire tx_fifo_empty;
        wire [7:0]tx_data_out;
        
        packet_fifo tx_fifo(.clk(RXC), .data_wr(tx_fifo_wr), .data_in(tx_din),
                        .data_valid(tx_fifo_dv), .data_cancel(tx_fifo_dc), 
                        .data_rd(tx_fifo_rd), .packet_done(tx_frame_done), .data_out(tx_data_out),
                        .fifo_empty(tx_fifo_empty), .fifo_full(tx_fifo_full));  
          
        reg [7:0] tx_data = 0;
        reg crc_rst = 1;
        reg [31:0]crc_r = 0;
        wire [31:0]crc_w;
        
        eth_crc32x8 tx_crc(.clk(RXC), .data(tx_data_out), .rst(crc_rst), .crc(crc_w));
        
        reg [15:0]tx_fsm_cnt = 0;
        
        localparam TX_IDLE          = 0;
        localparam TX_PREAMBLE      = 1;
        localparam TX_SFD           = 2;
        localparam TX_DATA          = 3;
        localparam TX_CRC           = 4;
        
        reg [2:0]tx_state = TX_IDLE;
        
        always @(posedge RXC)
        begin
            gmii_txen <= 0;
            gmii_txer <= 0;
            crc_rst <= 1;
            
            tx_data <= tx_data_out;
            tx_fifo_rd <= 0;
            
            tx_fsm_cnt <= 0;
            
            case(tx_state)
                TX_IDLE: begin
                    if(!tx_fifo_empty) begin
                        tx_state <= TX_PREAMBLE;
                    end
                end
                
                TX_PREAMBLE: begin
                    tx_fsm_cnt <= tx_fsm_cnt + 16'd1;
                    gmii_txen <= 1;
                    gmii_txd <= ETH_PREAMBLE;
                    if(tx_fsm_cnt == 7) begin
                        tx_fsm_cnt <= 0;
                        tx_state <= TX_SFD;
                        tx_fifo_rd <= 1; // testing start reading before
                    end
                end
                
                TX_SFD: begin
                    gmii_txen <= 1;
                    gmii_txd <= ETH_SFD;
                    // Start the CRC calculation one cycle earlier
                    // we push data in CRC and increment counter
                    crc_rst <= 0;
                    tx_fifo_rd <= 1;
                    tx_state <= TX_DATA;
                end
                
                TX_DATA: begin
                    gmii_txen <= 1;
                    gmii_txd <= tx_data_out;
                    
                    crc_rst <= 0;
                    tx_fifo_rd <= 1;
                    
                    if(tx_frame_done) begin
                        tx_state <= TX_CRC;
                        tx_fsm_cnt <= 0;
                        tx_fifo_rd <= 0;
                    end
                end
                
                TX_CRC: begin
                    gmii_txen <= 1;
                    crc_r <= {crc_w[23:0], crc_w[31:24]};
                    gmii_txd <= crc_w[31:24];
                    tx_fsm_cnt <= tx_fsm_cnt + 16'd1;
                    if(tx_fsm_cnt) begin
                        crc_r <= {crc_r[23:0], crc_r[31:24]};
                        gmii_txd <= crc_r[31:24];
                        if(tx_fsm_cnt == 3) tx_state <= TX_IDLE;
                    end
                end
            endcase
        end
endmodule
