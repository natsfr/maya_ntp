`timescale 1ns / 1ps
`default_nettype none

module frame_processor(
    // RGMII Interface
    input wire RXC,
    input wire [3:0]RXD,
    input wire RXCTL,
    
    output wire TXC,
    output wire [3:0]TXD,
    output wire TXCTL,
    
    // Optionnal Later MII bridge
    
    
    output reg ledb = 0,
    output reg ledr = 0,
    output reg ledg = 0
    );
    
    parameter FIFO_SIZE = 16'd16384;
    parameter WAIT_TIME = 27'd125000000;
    //parameter WAIT_TIME = 27'd10;
    
    reg tx_write = 0;
    reg tx_dv = 0;
    reg tx_dc = 0;
    reg [7:0]tx_data = 0;
    wire tx_full;
    
    reg tx_last_byte = 0;
    
    reg rx_read = 0;
    wire [7:0]rx_data;
    wire rx_empty;
    wire rx_done;
    
    // Need to write an AXI interface for the MAC as it is
    
    mac_txer test_loopback(
                .tx_fifo_wr(tx_write), .tx_fifo_dv(tx_dv), .tx_fifo_dc(tx_dc),
                .tx_din(tx_data), .tx_fifo_full(tx_full),
                .rx_fifo_rd(rx_read), .rx_data_out(rx_data), .rx_fifo_empty(rx_empty), .rx_frame_done(rx_done),
                .RXC(RXC), .RXD(RXD), .RXCTL(RXCTL), .TXC(TXC), .TXD(TXD), .TXCTL(TXCTL), .ledr(),
                .prom_mode(1), .MAC(47'h00DEADBEEF00));
    
    reg [7:0]arp_frame [0:59];
    reg [5:0]arp_index = 0;
    
    reg [26:0]sec_cnt = 0;
    reg sec_trigged = 0;
    reg sec_reset = 0;

    always @(posedge RXC)
    begin
        if(sec_cnt == WAIT_TIME)
            sec_trigged <= 1;
        else
            sec_cnt <= sec_cnt + 27'd1;
        if(sec_reset) begin
            sec_cnt <= 27'd0;
            sec_trigged <= 0;
        end
    end
    
    localparam ETH_TYPE_ARP     = 16'h0806;
    localparam MY_IP            = 32'h0A000001; // 10.0.0.1
    
    // sender and destination mac
    reg [47:0]s_mac = 0;
    reg [47:0]r_mac = 0;
    
    reg [31:0]s_ip = 0;
    reg [31:0]r_ip = 0;
    
    reg [15:0]ethtype = 0;
    
        // ARP
        reg [15:0]hwtype = 0;
        reg [15:0]prottype = 0;
        reg [7:0]hlen = 0;
        reg [7:0]plen = 0;
        reg [15:0]oper = 0;
        
    // FSM reg
    reg [7:0]fsm_cnt = 0;
    
    localparam IDLE             = 0;
    localparam READ_R_MAC       = 1;
    localparam READ_S_MAC       = 2;
    localparam READ_ETH_TYPE    = 3;
    localparam SWITCH_PROT      = 4;
    
    reg [2:0]state = IDLE;
    
    always @(posedge RXC)
    begin
        rx_read <= 0;
        tx_write <= 0;
        tx_dv <= 0;
        tx_dc <= 0;
        
        arp_index <= 0;
        sec_reset <= 0;
        
        tx_last_byte <= 0;
        
        ledg <= 0;
        ledr <= 0;
        
        case(state)
            IDLE: begin
                if(!rx_empty) begin
                    rx_read <= 1;
                    if(rx_read) state <= READ_R_MAC; // We wait one more cycle :)
                end
            end
            
            READ_R_MAC: begin
                rx_read <= 1;
                fsm_cnt <= fsm_cnt + 1;
                
                r_mac <= {r_mac[39:0], rx_data};
                
                if(fsm_cnt == 5) begin
                    state <= READ_S_MAC;
                    fsm_cnt <= 0;
                end
            end
            
            READ_S_MAC: begin
                rx_read <= 1;
                fsm_cnt <= fsm_cnt + 1;
                
                s_mac <= {s_mac[39:0], rx_data};
                
                if(fsm_cnt == 5) begin
                    state <= READ_S_MAC;
                    fsm_cnt <= 0;
                end
            end
            
            READ_ETH_TYPE: begin
                rx_read <= 1;
                fsm_cnt <= fsm_cnt + 1;
                
                ethtype <= {ethtype[7:0], rx_data};
                
                if(fsm_cnt == 1) begin
                    state <= SWITCH_PROT;
                    fsm_cnt <= 0;
                end
            end
            
            SWITCH_PROT: begin
                rx_read <= 1;
                case(ethtype)
                    ETH_TYPE_ARP: begin
                        hwtype[15:8] <= rx_data;
                    end
                    
                endcase
            end
            
        endcase
    end
    
    initial begin
        $readmemh("arp_grat.data", arp_frame);
    end
    
endmodule
