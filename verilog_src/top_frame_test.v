`timescale 1ns / 1ps
`default_nettype none

module top_frame_proc(
	// RGMII Interface
	input wire RXC,
	input wire [3:0]RXD,
	input wire RXCTL,
	
	output wire TXC,
	output wire [3:0]TXD,
	output wire TXCTL,
	
	output wire mdc,
	
	input wire SFP_ABS,
	output wire SFP_TX_DIS,
	output wire SFP_PW_EN,
	output wire nreset,
	
	output wire led_b,
	output wire led_g,
	output wire led_r
	
	);
	
	frame_processor test_frame(.RXC(RXC), .RXD(RXD), .RXCTL(RXCTL), .TXC(TXC), .TXD(TXD), .TXCTL(TXCTL), .ledb(led_b), .ledr(led_r), .ledg(led_g));
	
	// We start the phy
	assign nreset = 1;
	
	assign mdc = 0;
	
	assign SFP_TX_DIS = 0;
	assign SFP_PW_EN = !SFP_ABS;
	//assign led_g = !SFP_ABS;
    
endmodule
