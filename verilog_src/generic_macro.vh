`ifndef _generic_macro_h
`define _generic_macro_h

/* LOG base 2 for range calculation */
`define CLOG2(x) \
   (x <= 2) ? 1 : \
   (x <= 4) ? 2 : \
   (x <= 8) ? 3 : \
   (x <= 16) ? 4 : \
   (x <= 32) ? 5 : \
   (x <= 64) ? 6 : -1;

`endif