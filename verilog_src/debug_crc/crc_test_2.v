module eth_crc_8(clk, rst, crc_en, data, crc);
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// I/O declarations
	input wire clk;
	input wire rst;
	input wire crc_en;
	input wire[7:0] data;
    
	reg[31:0] crc_r = 32'hffffffff;
    
	output wire[31:0] crc;
    
	wire[31:0] crc_not = ~crc_r;
    
	assign crc =
	{
		crc_not[24], crc_not[25], crc_not[26], crc_not[27],
		crc_not[28], crc_not[29], crc_not[30], crc_not[31],
		crc_not[16], crc_not[17], crc_not[18], crc_not[19],
		crc_not[20], crc_not[21], crc_not[22], crc_not[23],
		crc_not[8], crc_not[9], crc_not[10], crc_not[11],
		crc_not[12], crc_not[13], crc_not[14], crc_not[15],
		crc_not[0], crc_not[1], crc_not[2], crc_not[3],
		crc_not[4], crc_not[5], crc_not[6], crc_not[7]		
	};
    
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// The actual CRC function
	wire[7:0] din_flipped = 
	{
		data[0], data[1], data[2], data[3],
		data[4], data[5], data[6], data[7]
	};
    
	always @(posedge clk) begin
		if(rst)
			crc_r <= 'hffffffff;
		if(crc_en) begin
			crc_r[0] <= din_flipped[6] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[30];
			crc_r[1] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[25] ^ crc_r[30] ^ crc_r[31];
			crc_r[2] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[2] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[25] ^ crc_r[26] ^ crc_r[30] ^ crc_r[31];
			crc_r[3] <= din_flipped[7] ^ din_flipped[3] ^ din_flipped[2] ^ din_flipped[1] ^ crc_r[25] ^ crc_r[26] ^ crc_r[27] ^ crc_r[31];
			crc_r[4] <= din_flipped[6] ^ din_flipped[4] ^ din_flipped[3] ^ din_flipped[2] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[26] ^ crc_r[27] ^ crc_r[28] ^ crc_r[30];
			crc_r[5] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[5] ^ din_flipped[4] ^ din_flipped[3] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[25] ^ crc_r[27] ^ crc_r[28] ^ crc_r[29] ^ crc_r[30] ^ crc_r[31];
			crc_r[6] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[5] ^ din_flipped[4] ^ din_flipped[2] ^ din_flipped[1] ^ crc_r[25] ^ crc_r[26] ^ crc_r[28] ^ crc_r[29] ^ crc_r[30] ^ crc_r[31];
			crc_r[7] <= din_flipped[7] ^ din_flipped[5] ^ din_flipped[3] ^ din_flipped[2] ^ din_flipped[0] ^ crc_r[24] ^ crc_r[26] ^ crc_r[27] ^ crc_r[29] ^ crc_r[31];
			crc_r[8] <= din_flipped[4] ^ din_flipped[3] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[0] ^ crc_r[24] ^ crc_r[25] ^ crc_r[27] ^ crc_r[28];
			crc_r[9] <= din_flipped[5] ^ din_flipped[4] ^ din_flipped[2] ^ din_flipped[1] ^ crc_r[1] ^ crc_r[25] ^ crc_r[26] ^ crc_r[28] ^ crc_r[29];
			crc_r[10] <= din_flipped[5] ^ din_flipped[3] ^ din_flipped[2] ^ din_flipped[0] ^ crc_r[2] ^ crc_r[24] ^ crc_r[26] ^ crc_r[27] ^ crc_r[29];
			crc_r[11] <= din_flipped[4] ^ din_flipped[3] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[3] ^ crc_r[24] ^ crc_r[25] ^ crc_r[27] ^ crc_r[28];
			crc_r[12] <= din_flipped[6] ^ din_flipped[5] ^ din_flipped[4] ^ din_flipped[2] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[4] ^ crc_r[24] ^ crc_r[25] ^ crc_r[26] ^ crc_r[28] ^ crc_r[29] ^ crc_r[30];
			crc_r[13] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[5] ^ din_flipped[3] ^ din_flipped[2] ^ din_flipped[1] ^ crc_r[5] ^ crc_r[25] ^ crc_r[26] ^ crc_r[27] ^ crc_r[29] ^ crc_r[30] ^ crc_r[31];
			crc_r[14] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[4] ^ din_flipped[3] ^ din_flipped[2] ^ crc_r[6] ^ crc_r[26] ^ crc_r[27] ^ crc_r[28] ^ crc_r[30] ^ crc_r[31];
			crc_r[15] <= din_flipped[7] ^ din_flipped[5] ^ din_flipped[4] ^ din_flipped[3] ^ crc_r[7] ^ crc_r[27] ^ crc_r[28] ^ crc_r[29] ^ crc_r[31];
			crc_r[16] <= din_flipped[5] ^ din_flipped[4] ^ din_flipped[0] ^ crc_r[8] ^ crc_r[24] ^ crc_r[28] ^ crc_r[29];
			crc_r[17] <= din_flipped[6] ^ din_flipped[5] ^ din_flipped[1] ^ crc_r[9] ^ crc_r[25] ^ crc_r[29] ^ crc_r[30];
			crc_r[18] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[2] ^ crc_r[10] ^ crc_r[26] ^ crc_r[30] ^ crc_r[31];
			crc_r[19] <= din_flipped[7] ^ din_flipped[3] ^ crc_r[11] ^ crc_r[27] ^ crc_r[31];
			crc_r[20] <= din_flipped[4] ^ crc_r[12] ^ crc_r[28];
			crc_r[21] <= din_flipped[5] ^ crc_r[13] ^ crc_r[29];
			crc_r[22] <= din_flipped[0] ^ crc_r[14] ^ crc_r[24];
			crc_r[23] <= din_flipped[6] ^ din_flipped[1] ^ din_flipped[0] ^ crc_r[15] ^ crc_r[24] ^ crc_r[25] ^ crc_r[30];
			crc_r[24] <= din_flipped[7] ^ din_flipped[2] ^ din_flipped[1] ^ crc_r[16] ^ crc_r[25] ^ crc_r[26] ^ crc_r[31];
			crc_r[25] <= din_flipped[3] ^ din_flipped[2] ^ crc_r[17] ^ crc_r[26] ^ crc_r[27];
			crc_r[26] <= din_flipped[6] ^ din_flipped[4] ^ din_flipped[3] ^ din_flipped[0] ^ crc_r[18] ^ crc_r[24] ^ crc_r[27] ^ crc_r[28] ^ crc_r[30];
			crc_r[27] <= din_flipped[7] ^ din_flipped[5] ^ din_flipped[4] ^ din_flipped[1] ^ crc_r[19] ^ crc_r[25] ^ crc_r[28] ^ crc_r[29] ^ crc_r[31];
			crc_r[28] <= din_flipped[6] ^ din_flipped[5] ^ din_flipped[2] ^ crc_r[20] ^ crc_r[26] ^ crc_r[29] ^ crc_r[30];
			crc_r[29] <= din_flipped[7] ^ din_flipped[6] ^ din_flipped[3] ^ crc_r[21] ^ crc_r[27] ^ crc_r[30] ^ crc_r[31];
			crc_r[30] <= din_flipped[7] ^ din_flipped[4] ^ crc_r[22] ^ crc_r[28] ^ crc_r[31];
			crc_r[31] <= din_flipped[5] ^ crc_r[23] ^ crc_r[29];
		end
	end
endmodule