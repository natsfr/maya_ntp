`default_nettype none

module top_crc();

    reg clk = 0;
    always #1 clk <= ~clk;

    reg [7:0]ram_to_crc [0:63];
    
    reg [31:0]cnt = 0;
    
    reg [7:0]data_in = 0;
    reg crc_rst = 1;
    reg crc_en = 0;
    wire [31:0]crc_w;
    
    eth_crc_8_no_dreverse mycrc(.clk(clk), /*.crc_en(crc_en),*/ .data(data_in), .rst(crc_rst), .crc(crc_w));
    
    always @(posedge clk)
    begin
        crc_rst <= 0;
        crc_en <= 1;
        data_in <= ram_to_crc[cnt];
        $strobe("%d - %d - %d - %h - %h", crc_rst, crc_en, cnt, crc_w, data_in);
        cnt <= cnt + 1;
        if(cnt == 70) $finish;
    end
    
    initial
    begin
        $readmemh("arping_crc.data", ram_to_crc);
    end
    
endmodule