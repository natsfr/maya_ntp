module MDIO_transceiver(clk,
						mdio, mdc,
						phy_rd, phy_wr,
						mdio_busy, reg_addr, data_wr, data_rd);
	
	parameter PHY_ADDR			= 5'd0;
	parameter CLK_SPEED			= 50000000;
	parameter MDIO_FREQ			= 1000000;
	localparam CNT_CLK			= (CLK_SPEED / MDIO_FREQ)/2;
	
	// IO Declarations
	input wire clk;
	
	inout wire mdio;
	output reg mdc = 0;
	
	input wire phy_rd;
	input wire phy_wr;
	
	output reg mdio_busy = 0;
	input wire [4:0]reg_addr;
	input wire [15:0]data_wr;
	output reg [15:0]data_rd = 0;
	
	// TRI STATE MDIO
	wire bit_in;
	reg bit_out = 0;
	reg mdio_oe = 0;
	
	assign mdio = mdio_oe ? bit_out : 1'bz;
	assign bit_in = mdio;
	
	// Generate Edge
	reg [15:0]mdc_cnt = 0;
	reg mdc_r_edge = 0;
	reg mdc_f_edge = 0;
	
	always @(posedge clk)
	begin
		mdc_r_edge <= 0;
		mdc_f_edge <= 0;
		
		mdc_cnt <= mdc_cnt + 6'd1;
		
		if(mdc_cnt == CNT_CLK - 1) begin
			mdc_cnt <= 0;
			if(mdc == 1'b1)
				mdc_f_edge <= 1;
			else
				mdc_r_edge <= 1;
		end
		
		if(mdc_f_edge == 1'b1)
			mdc <= 0;
		
		if(mdc_r_edge == 1'b1)
			mdc <= 1;
	end
	
	// MDIO FSM and internal states
	
	reg read = 0;
	
	reg [4:0]reg_addr_l = 0;
	reg [15:0]data_wr_l = 0;
	
	reg [15:0]data_rd_buf = 0;
	
	reg [7:0]fsm_cnt = 0;
	
	localparam MDIO_IDLE			= 0;
	localparam MDIO_PRE			= 1;
	localparam MDIO_START		= 2;
	localparam MDIO_OP			= 3;
	localparam MDIO_PHY_ADDR	= 4;
	localparam MDIO_REG_ADDR	= 5;
	localparam MDIO_TA			= 6;
	localparam MDIO_DATA			= 7;
	localparam MDIO_IF			= 8;
	
	reg [3:0]mdio_fsm_state = MDIO_IDLE;
	
	always @(posedge clk)
	begin
		case(mdio_fsm_state)
			MDIO_IDLE: begin
				mdio_oe <= 0;
				read <= 0;
				fsm_cnt <= 0;
				mdio_busy <= 0;
				data_rd_buf <= 0;
				if(phy_rd) begin
					mdio_busy <= 1;
					reg_addr_l <= reg_addr;
					read <= 1;
					mdio_fsm_state <= MDIO_PRE;
				end else if(phy_wr) begin
					mdio_busy <= 1;
					reg_addr_l <= reg_addr;
					data_wr_l <= data_wr;
					mdio_fsm_state <= MDIO_PRE;
				end
			end
			
			MDIO_PRE: begin
				if(mdc_f_edge) begin
					mdio_oe <= 1;
					bit_out <= 1;
					fsm_cnt <= fsm_cnt + 8'd1;
					if(fsm_cnt == 8'd32) begin
						fsm_cnt <= 8'd0;
						mdio_fsm_state <= MDIO_START;
					end
				end
			end
			
			MDIO_START: begin
				if(mdc_f_edge) begin
					mdio_oe <= 1;
					bit_out <= fsm_cnt[0];
					fsm_cnt <= fsm_cnt + 8'd1;
					if(fsm_cnt == 8'd1) begin
						fsm_cnt <= 8'd0;
						mdio_fsm_state <= MDIO_OP;
					end
				end
			end
			
			MDIO_OP: begin
				if(mdc_f_edge) begin
					mdio_oe <= 1;
					if(read)
						bit_out <= ~fsm_cnt[0];
					else
						bit_out <= fsm_cnt[0];
					fsm_cnt <= fsm_cnt + 8'd1;
					if(fsm_cnt == 8'd1) begin
						fsm_cnt <= 0;
						mdio_fsm_state <= MDIO_PHY_ADDR;
					end
				end
			end
			
			MDIO_PHY_ADDR: begin
				mdio_oe <= 1;
				if(mdc_f_edge) begin
					fsm_cnt <= fsm_cnt + 8'd1;
					bit_out <= PHY_ADDR[4-fsm_cnt];
					if(fsm_cnt == 8'd4) begin
						fsm_cnt <= 0;
						mdio_fsm_state <= MDIO_REG_ADDR;
					end
				end
			end
			
			MDIO_REG_ADDR: begin
				mdio_oe <= 1;
				if(mdc_f_edge) begin
					fsm_cnt <= fsm_cnt + 8'd1;
					bit_out <= reg_addr_l[4-fsm_cnt];
					if(fsm_cnt == 8'd4) begin
						fsm_cnt <= 0;
						mdio_fsm_state <= MDIO_TA;
					end
				end
			end
			
			MDIO_TA: begin
				if(mdc_f_edge) begin
					if(read) begin
						// We release the line for the turn around
						mdio_oe <= 0;
					end else begin
						// We drive a 1 and a 0
						mdio_oe <= 1;
						bit_out <= ~fsm_cnt[0];
					end
					if(fsm_cnt == 0)
						fsm_cnt <= 1;
					else begin
						fsm_cnt <= 0;
						mdio_fsm_state <= MDIO_DATA;
					end
				end
			end
			
			MDIO_DATA: begin
				if(read) begin
					// We have to read 17 bits because we start
					// half a cycle earlier
					if(mdc_r_edge) begin
						mdio_oe <= 0;
						data_rd_buf <= {data_rd_buf[14:0], bit_in};
						fsm_cnt <= fsm_cnt + 8'd1;
						if(fsm_cnt == 8'd16) begin
							data_rd <= {data_rd_buf[14:0], bit_in};
							fsm_cnt <= 0;
							mdio_fsm_state <= MDIO_IF;
						end
					end
				end else begin
					if(mdc_f_edge) begin
						mdio_oe <= 1;
						bit_out <= data_wr_l[15-fsm_cnt];
						fsm_cnt <= fsm_cnt + 8'd1;
						if(fsm_cnt == 8'd15) begin
							fsm_cnt <= 0;
							mdio_fsm_state <= MDIO_IF;
						end
					end
				end
			end
			
			MDIO_IF: begin
				if(mdc_f_edge) begin
					fsm_cnt <= fsm_cnt + 8'd1;
					mdio_oe <= 0;
					read <= 0;
					if(fsm_cnt == 8'd15)begin
						mdio_fsm_state <= MDIO_IDLE;
						fsm_cnt <= 0;
						mdio_busy <= 0;
					end
				end
			end
			
		endcase
	end
	
endmodule